<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pengaduan;
use App\Models\Masyarakat;
use App\Models\Petugas;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function index() {
        $this_year = Carbon::now()->format('Y');
        $all = Pengaduan::where('tgl_pengaduan','like', $this_year.'%')->get();
        $done = Pengaduan::where('tgl_pengaduan','like', $this_year.'%')->where('status','selesai')->get();

        for ($i=1; $i <= 12; $i++){
            $data_month_all[(int)$i]=0;
            $data_month_done[(int)$i]=0;
        }

        foreach ($all as $a) {
            // $data=carbon::parse($a->$tgl_pengaduan)->format('Y-m-d');
            $bulan= explode('-',carbon::parse($a->tgl_pengaduan)->format('Y-m-d'))[1];
            $data_month_all[(int) $bulan]+=1;
        }

        foreach ($done as $d) {
            // $data=carbon::parse($a->$tgl_pengaduan)->format('Y-m-d');
            $month= explode('-',carbon::parse($d->tgl_pengaduan)->format('Y-m-d'))[1];
            $data_month_done[(int) $month]+=1;
        }
        return view('pages.admin.dashboard', [
            'pengaduan' => Pengaduan::count(),
            'proses' => Pengaduan::where('status', 'proses')->count(),
            'selesai' => Pengaduan::where('status', 'selesai')->count(),
            // 'masyarakat' => Masyarakat::count(),
            'masyarakat' => Masyarakat::count(),
        ])
        -> with('data_month_all', $data_month_all)    
        -> with('data_month_done', $data_month_done);
    }
    // public function editProfile(Request $request,$id) {
    //     // dd($request);
    //     $data = Petugas::findOrFail($id);

        
    //     $data->nama_petugas = $request->name;
    //     $data->username = $request->username;
    //     $data->update();
    //     // dd($data);

    //     return redirect()->back()->with('success', 'success change profile');
    // }

   public function editProfile(Request $request,$id) {

        $data = User::findOrFail($id);
        // dd($data);
        $data->photo = $request->file('photo');
        if ($request->file('photo')) {
            $data->photo = $request->file('photo')->store('assets/pp', 'public');
        }
        // if ($request->hasFile('photo')) {
        //     $masyarakat=Masyarakat::select('name')->where('id',$id)->first();
        //     $filename = $masyarakat->name.'.png';
        //     $request->file('photo')->store('assets/pp',$filename,'public');
        // }
        $data->name = $request->name;
        $data->username = $request->username;
        // dd($data);
        $data->update();

        return redirect()->back()->with('success', 'success change profile');
    }
}
