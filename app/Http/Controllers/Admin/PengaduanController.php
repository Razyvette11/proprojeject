<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Masyarakat;
use Illuminate\Http\Request;
use App\Models\Pengaduan;
use App\Models\Tanggapan;
use Carbon\CarbonPeriod;
use Illuminate\Support\Carbon;

class PengaduanController extends Controller
{
    public function index($status) {
        $user = Masyarakat::all();
        $pengaduan = Pengaduan::where('status', $status)->orderBy('tgl_pengaduan', 'desc')->get();
        // dd($pengaduan);
        return view('pages.admin.pengaduan.index', compact('pengaduan', 'status'));
    }
 
    public function show($id_pengaduan) {
        $pengaduan = Pengaduan::find($id_pengaduan);
        $tanggapan = Tanggapan::find($id_pengaduan);
        $user = Masyarakat::all();
        

        return view('pages.admin.pengaduan.show', [
            'pengaduan' => $pengaduan,
            'tanggapan' => $tanggapan,
            'user' => $user,
        ]);
    }

    public function destroy(Request $request, $id_pengaduan) {

        if($id_pengaduan = 'id_pengaduan') {
            $id_pengaduan = $request->id_pengaduan;
        }

        $pengaduan = Pengaduan::find($id_pengaduan);

        $pengaduan->delete();

        if($request->ajax()) {
            return 'success';
        }

        return redirect()->route('pengaduan.index');
    }
}
