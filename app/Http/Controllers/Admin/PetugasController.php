<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\Petugas;
Use Alert;
use Illuminate\Validation\Rule;
use App\Models\User;
use App\Models\RoleUser;

class PetugasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $petugas = Petugas::all();

        return view('pages.admin.petugas.index', compact('petugas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.petugas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validate = Validator::make($data, [
            'nama_petugas' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'regex:/^\S*$/u', 'unique:users'],
            'password' => ['required', 'string', 'min:6'],
            'telp' => ['required', 'regex:/(08)[0-9]/'],
            'roles' => ['required', 'in:2,3'],
        ]);

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate);
        };

        $username = User::where('username', $data['username'])->first();

        if($username){
            return redirect()->back()->with(['notif' => 'Username Telah Digunakan!']);
        }

        $user = User::create([
            'name' => $data['nama_petugas'],
            'username' => $data['username'],
            'password' => Hash::make($data['password']),
            ]);
            
            RoleUser::create([
                'user_id' => $user->id,
                'role_id' => $request->roles,
            ]);    
            Petugas::create([
                'nama_petugas' => $data['nama_petugas'],
                'telp' => $data['telp'],
                'user_id' => $user->id
        ]);


        Alert::success('Berhasil', 'Petugas telah ditambahkan!');
        return redirect()->route('admin.petugas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //\
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_petugas)
    {
        // dd($id_petugas);
        $petugas = Petugas::where('user_id', $id_petugas)->first();

        return view('pages.admin.petugas.edit', compact('petugas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_petugas)
    {
        $data = $request->all();
        // dd($data);

        // $validate = Validator::make($data, [
        //     'nama_petugas' => ['required', 'string', 'max:255'],
        //     'username' => ['required', 'string', 'regex:/^\S*$/u', Rule::unique('users')],
        //     'telp' => ['required'],
        //     'roles' => ['required', 'in:2,3'],
        // ]);

        // if ($validate->fails()) {
        //     return redirect()->back()->withErrors($validate);
        // };
        

        $petugas = Petugas::where('user_id',$id_petugas)->first();
        $role_user = RoleUser::where('user_id',$id_petugas)->first();
        $user = User::find($id_petugas);

        // dd($petugas);


        if($data['password'] != null){
            $password = Hash::make($data['password']);
        }

        $update_petugas = $petugas->update([
            'nama_petugas' => $data['nama_petugas'],
            'telp' => $data['telp'],
        ]);

        if ($update_petugas == true){
            $update_role_user = $role_user->update([
                'role_id' => $data['roles']
            ]);
        }        
        else (Alert::danger('Gagal' , 'Petugas gagal diupdate!'));

        if ($update_role_user == true){
            $user->update([
                'name' => $data['nama_petugas'],
                'username' => strtolower($data['username']),
                'password' => $password ?? $user->password,
            ]);
            Alert::success('Berhasil', 'Petugas berhasil diupdate!');
        }
        else (Alert::danger('Gagal' , 'Petugas gagal diupdate!'));
        return redirect()->route('admin.petugas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $id_petugas = $request->id;

        $petugas = Petugas::where('user_id', $id_petugas)->first();
        // dd($petugas);
        
        $petugas->delete();

        if($request->ajax()) {
            return 'success';
        }

        return redirect()->route('admin.petugas.index');
    }
}


