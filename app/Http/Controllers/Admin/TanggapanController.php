<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Pengaduan;
use App\Models\Tanggapan;
use App\Models\Upload;

class TanggapanController extends Controller
{
    public function response(Request $request) {
        // dd($request->all());
        $pengaduan = Pengaduan::where('id_pengaduan', $request->id_pengaduan)->first();
        $tanggapan = Tanggapan::where('id_pengaduan', $request->id_pengaduan)->first();
        
        if($tanggapan) {
            $pengaduan->update(['status' => $request->status]);
            $date = date("Y-m-d H:i:s");
            $time = strtotime($date);
            $upload_id = "";
            if($request->foto){
                $upload_id = $time . Auth::user()->id . Auth::user()->role[0]->id;

                for ($i = 0; $i < count($request["foto"]); $i++) {
    
                        $nama_file = $request->file('foto')[$i]->store('assets/tanggapan', 'public');
                    
                    $upload = Upload::create([
                        'photo' => $nama_file,
                        'upload_id' => $upload_id
                    ]);
    
                }
            }
            $tanggapan->update([
                'tgl_tanggapan' => date('Y-m-d'),
                'tanggapan' => $request->tanggapan ?? '',
                'id_petugas' => Auth::user()->id,
                'upload_id' => $upload_id 
            ]);

            if($request->ajax()) {
                return 'success';
            }

            return redirect()->route('pengaduan.show', ['id_pengaduan' => $request->id_pengaduan, 'pengaduan' => $pengaduan, 'tanggapan' => $tanggapan])->with(['status' => 'Berhasil Ditanggapi!']);
        } else {

            $pengaduan->update(['status' => $request->status]);
            $date = date("Y-m-d H:i:s");
            $time = strtotime($date);
            $upload_id = "";
            //Gara2 ini kayaknya, di controller upload sesuatu, padahal user ga upload apa2
            if($request->foto){
                $upload_id = $time . Auth::user()->id . Auth::user()->role[0]->id;

                for ($i = 0; $i < count($request["foto"]); $i++) {
    
                        $nama_file = $request->file('foto')[$i]->store('assets/tanggapan', 'public');
                    
                    $upload = Upload::create([
                        'photo' => $nama_file,
                        'upload_id' => $upload_id
                    ]);
    
                }
            }
            
            $tanggapan = Tanggapan::create([
                'id_pengaduan' => $request->id_pengaduan,
                'tgl_tanggapan' => date('Y-m-d'),
                'tanggapan' => $request->tanggapan ?? '',
                'admin_id' => Auth::user()->id,
                'upload_id' => $upload_id 
                
            ]);
            if($request->ajax()) {
                return 'success';
            }

            return redirect()->route('pengaduan.show', ['id_pengaduan' => $request->id_pengaduan, 'pengaduan' => $pengaduan, 'tanggapan' => $tanggapan])->with(['status' => 'Berhasil Ditanggapi!']);
        }
    }
}

