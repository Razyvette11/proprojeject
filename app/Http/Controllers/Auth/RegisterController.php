<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\RoleUser;
use App\Models\Masyarakat;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = "masyarakat/home";

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'telp' => ['required', 'regex:/(08)[0-9]/','max:13', 'min:10'],
            'address' => ['required'],
            'username' => ['required', 'string','regex:/^\S*$/u', 'max:50', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'max:6', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'password' => Hash::make($data['password']),
            ]);
            
            RoleUser::create([
                'user_id' => $user->id,
                'role_id' => 1,
            ]);
            
            Masyarakat::create([
                'name' => $data['name'],
                'telp' => $data['telp'],
                'address' => $data['address'],
                'user_id' => $user->id,
                ]);

        return $user;
    }

    public function showRegistrationForm()
    {
        return view('pages.masyarakat.register');
    }
}
