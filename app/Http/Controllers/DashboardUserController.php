<?php

namespace App\Http\Controllers  ;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pengaduan;
use App\Models\Masyarakat;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class DashboardUserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
            $pengaduan = Pengaduan::where('user_id', Auth::user()->id)->orderBy('tgl_pengaduan', 'desc')->count();
        // $pengaduans = Pengaduan::where('id', Auth::guard('masyarakat')->user()->id)->orderBy('tgl_pengaduan', 'desc')->get();
        $proses = Pengaduan::where('user_id', Auth::user()->id)->orderBy('tgl_pengaduan', 'desc')->where('status', 'proses')->count();
        $selesai = Pengaduan::where('user_id', Auth::user()->id)->orderBy('tgl_pengaduan', 'desc')->where('status', 'selesai')->count();
        $mas = Masyarakat::count();
        $aduan = Pengaduan::where('user_id', Auth::user()->id)->orderBy('tgl_pengaduan', 'desc')->get();
        // dd($aduan);
        // $mas = Masyarakat::select('rt')->first();

        return view('pages.masyarakat.dashboard', [
            'pengaduan' => $pengaduan,
            // 'pengaduans' => $pengaduans,
            'proses' => $proses,
            'selesai' => $selesai,
            'mas' => $mas,
            'aduan' => $aduan
        ]);
        
    }

    public function admin()
    {
        $pengaduan = Pengaduan::count();
        $proses = Pengaduan::where('status','proses')->count();
        $selesai = Pengaduan::where('status','selesai')->count();
        $masyarakat = Masyarakat::count();
        
            $this_year = Carbon::now()->format('Y');
            $all = Pengaduan::where('tgl_pengaduan','like', $this_year.'%')->get();
            $done = Pengaduan::where('tgl_pengaduan','like', $this_year.'%')->where('status','selesai')->get();
    
            for ($i=1; $i <= 12; $i++){
                $data_month_all[(int)$i]=0;
                $data_month_done[(int)$i]=0;
            }
    
            foreach ($all as $a) {
                // $data=carbon::parse($a->$tgl_pengaduan)->format('Y-m-d');
                $bulan= explode('-',carbon::parse($a->tgl_pengaduan)->format('Y-m-d'))[1];
                $data_month_all[(int) $bulan]+=1;
            }
    
            foreach ($done as $d) {
                // $data=carbon::parse($a->$tgl_pengaduan)->format('Y-m-d');
                $month= explode('-',carbon::parse($d->tgl_pengaduan)->format('Y-m-d'))[1];
                $data_month_done[(int) $month]+=1;
            }

        return view('pages.admin.dashboard', [
            'pengaduan' => $pengaduan,
            'proses' => $proses,
            'selesai' => $selesai,
            'masyarakat' => $masyarakat,
            'data_month_all' => $data_month_all,
            'data_month_done' => $data_month_done
        ]);
    }

    public function laporan($who = '')
    {
        $terverifikasi = Pengaduan::where([['id', Auth::user()->id], ['status', '!=', '0']])->get()->count();
        $proses = Pengaduan::where([['id', Auth::user()->id], ['status', 'proses']])->get()->count();
        $selesai = Pengaduan::where([['id', Auth::user()->id], ['status', 'selesai']])->get()->count();
        // $aduan = Pengaduan::where('id_pengaduan', $id_pengaduan)->first();
        // $aduan = Pengaduan::all();

        $hitung = [$terverifikasi, $proses, $selesai];

        if ($who == 'saya') {

            $pengaduan = Pengaduan::where('id', Auth::user()->id)->orderBy('tgl_pengaduan', 'desc')->get();

            return view('pages.masyarakat.dashboard', [
                'pengaduan' => $pengaduan, 
                'hitung' => $hitung, 
                'who' => $who,
                // 'aduan' => $aduan
            ]);
        } else {

            $pengaduan = Pengaduan::where('status', '!=', '0')->orderBy('tgl_pengaduan', 'desc')->get();

            return view('pages.masyarakat.dashboard', [
                'pengaduan' => $pengaduan, 
                'hitung' => $hitung, 
                'who' => $who
            ]);
        }
    }

    public function editProfile(Request $request,$id) {

        $data = User::findOrFail($id);
        // dd($data);
        $data->photo = $request->file('photo');
        if ($request->file('photo')) {
            $data->photo = $request->file('photo')->store('assets/pp', 'public');
        }
        // if ($request->hasFile('photo')) {
        //     $masyarakat=Masyarakat::select('name')->where('id',$id)->first();
        //     $filename = $masyarakat->name.'.png';
        //     $request->file('photo')->store('assets/pp',$filename,'public');
        // }
        $data->name = $request->name;
        $data->username = $request->username;
        // dd($data);
        $data->update();

        return redirect()->back()->with('success', 'success change profile');
    }
}
