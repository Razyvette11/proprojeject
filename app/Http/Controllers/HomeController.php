<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Pengaduan;
use App\Models\Masyarakat;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()->role[0]->role_name == "Administrator"){
            return redirect("admin/home");
        }
        else if(Auth::user()->role[0]->role_name == "Petugas"){
            return redirect("petugas/home");
        }
        return redirect("masyarakat/home");
    }
}
