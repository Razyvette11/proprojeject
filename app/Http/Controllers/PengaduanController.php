<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pengaduan;
use App\Models\Masyarakat;
use App\Models\Tanggapan;
use Carbon\CarbonPeriod;
use Illuminate\Support\Carbon;

class PengaduanController extends Controller
{
    public function show($id_pengaduan) {
        $pengaduan = Pengaduan::where('id_pengaduan', $id_pengaduan)->first();
        $tanggapan = Tanggapan::where('id_pengaduan', $id_pengaduan)->first();
        $name = Masyarakat::all();


        return view('pages.admin.show', [
            'pengaduan' => $pengaduan,
            'tanggapan' => $tanggapan,
            'name' => $name,
        ]);
    }
}
