<?php

namespace App\Http\Controllers\Petugas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Masyarakat;
use App\Models\RoleUser;
use App\Models\User;

class MasyarakatController extends Controller
{
    public function index() {

        $masyarakat = Masyarakat::all();

        return view('pages.admin.masyarakat.index', compact('masyarakat'));
    }

    public function show($id) {
        
        $masyarakat = Masyarakat::where('id', $id)->first();

        return view('pages.admin.masyarakat.show', compact('masyarakat'));
    }

    public function destroy(Request $request){

        $masyarakat = Masyarakat::where('user_id',$request->id)->first();
        $role_user = RoleUser::where('user_id',$request->id)->first();
        $user = User::where('id',$request->id)->first();

        $masyarakat->delete();
        $role_user->delete();
        $user->delete();
        
        if($request->ajax()) {
            return 'success';
        }

        return redirect()->route('petugas.masyarakat.index');
    }
}
