<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Mail\VerifikasiEmailUntukRegistrasiPengaduanMasyarakat;
use App\Models\Masyarakat;
use App\Models\Pengaduan;
use App\Models\Petugas;
use App\Models\Kategori;
use App\Models\Tanggapan;
use App\Models\Upload;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function index()
    {
        $pengaduan = Pengaduan::count();
        $proses = Pengaduan::where('status', 'proses')->count();
        $selesai = Pengaduan::where('status', 'selesai')->count();

        return view('home', [
            'pengaduan' => $pengaduan,
            'proses' => $proses,
            'selesai' => $selesai,
        ]);
    }

    public function tentang()
    {
        return view('pages.masyarakat.about');
    }

    public function pengaduan()
    {
        $kategori = Kategori::get();
        return view('pages.masyarakat.pengaduan', compact('kategori'));
    }

    public function masuk()
    {
        return view('pages.masyarakat.login');
    }

    public function login(Request $request)
    {

        $data = $request->all();

        $validate = Validator::make($data, [
            'username' => ['required'],
            'password' => ['required']
        ]);

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }

        if (filter_var($request->username, FILTER_VALIDATE_EMAIL)) {


        } else {

            $masyarakat = User::where('username', $request->username)->first();

            $petugas = Petugas::where('username', $request->username)->first();

            if ($masyarakat) {
                $username = User::where('username', $request->username)->first();

                if (!$username) {
                    return redirect()->back()->with(['pesan' => 'Username tidak terdaftar']);
                }

                $password = Hash::check($request->password, $username->password);

                if (!$password) {
                    return redirect()->back()->with(['pesan' => 'Password tidak sesuai']);
                }

                if (Auth::attempt(['username' => $request->username, 'password' => $request->password])) {

                    return redirect()->route('masyarakat.home');
                } else {

                    return redirect()->back()->with(['pesan' => 'Akun tidak terdaftar!']);
                }
            } elseif ($petugas) {
                // dd("ini petugas");
                $username = User::where('username', $request->username)->first();
                // dd($username);

                if (!$username) {
                    return redirect()->back()->with(['pesan' => 'Username tidak terdaftar']);
                }

                $password = Hash::check($request->password, $username->password);

                if (!$password) {
                    return redirect()->back()->with(['pesan' => 'Password tidak sesuai']);
                }

                if (Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
                    // dd("masook");
                    return redirect()->route('dashboard');
                } else {

                    return redirect()->back()->with(['pesan' => 'Akun tidak terdaftar!']);
                }
            } else {
                return redirect()->back()->with(['pesan' => 'Akun tidak terdaftar!']);
            }
        }
    }

    public function register()
    {
        return view('pages.masyarakat.register');
    }

    public function register_post(Request $request)
    {
        $data = $request->all();
        // dd($data);
        $validate = Validator::make($data, [
            'name' => ['required', 'string'],
            'username' => ['required', 'string', 'regex:/^\S*$/u', 'unique:masyarakat', 'unique:petugas,username'],
            'password' => ['required', 'min:6'],
            'telp' => ['required', 'regex:/(08)[0-9]/'],
            'address' => ['required'],
        ]);

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }

        
        Masyarakat::create([
            'name' => $data['name'],
            'username' => strtolower($data['username']),
            'password' => Hash::make($data['password']),
            'telp' => $data['telp'],
            'address' => $data['address'],
        ]);

        $masyarakat = Masyarakat::where('name', $data['name'])->first();

        Auth::login($masyarakat);

        return redirect('/dashboard');
    }

    public function logout()
    {
        Auth::logout();

        return redirect('/');
    }

    public function storePengaduan(Request $request)
    {
        if (Auth::user()->role[0]->role_name != "Masyarakat")
        {
            return redirect()->back()->with(['pengaduan' => 'Login dibutuhkan!', 'type' => 'error']);
        // } elseif (Auth::user()->email_verified_at == null && Auth::user()->telp_verified_at == null) {
        //     return redirect()->back()->with(['pengaduan' => 'Akun belum diverifikasi!', 'type' => 'error']);
        }
        $data = $request->all();
        // dd($data);

        $validate = Validator::make($data, [
            'judul_laporan' => ['required'],
            'isi_laporan' => ['required'],
            'tgl_kejadian' => ['required'],
            'lokasi_kejadian' => ['required'],
            'id_kategori' => ['required'],
        ]);

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }


        if ($request->file('foto')) {
            $data['foto'] = $request->file('foto')->store('assets/pengaduan', 'public');
        }

        date_default_timezone_set('Asia/Bangkok');

        $pengaduan = Pengaduan::create([
            'name' => Auth::user()->name,
            'tgl_pengaduan' => date('Y-m-d h:i:s'),
            'judul_laporan' => $data['judul_laporan'],
            'isi_laporan' => $data['isi_laporan'],
            'tgl_kejadian' => $data['tgl_kejadian'],
            'lokasi_kejadian' => $data['lokasi_kejadian'],
            'id_kategori' => $data['id_kategori'],
            'foto' => $data['foto'] ?? 'assets/pengaduan/tambakmekar.png',
            'status' => 'pending',
            'user_id' => Auth::user()->id,
        ]); 

        if ($pengaduan) {

            return redirect()->back()->with(['pengaduan' => 'Berhasil terkirim!', 'type' => 'success']);
        } else {

            return redirect()->back()->with(['pengaduan' => 'Gagal terkirim!', 'type' => 'error']);
        }
    }

    public function laporan($who = '')
    {
        $terverifikasi = Pengaduan::where([['user_id', Auth::user()->id], ['status', '!=', 'pending']])->get()->count();
        $proses = Pengaduan::where([['user_id', Auth::user()->id], ['status', 'proses']])->get()->count();
        $selesai = Pengaduan::where([['user_id', Auth::user()->id], ['status', 'selesai']])->get()->count();

        $hitung = [$terverifikasi, $proses, $selesai];

        if ($who == 'saya') {

            $pengaduan = Pengaduan::where('user_id', Auth::user()->id)->orderBy('tgl_pengaduan', 'desc')->get();

            return view('pages.masyarakat.laporan', ['pengaduan' => $pengaduan, 'hitung' => $hitung, 'who' => $who]);
        } else {

            $pengaduan = Pengaduan::where('status', '!=', 'pending')->orderBy('tgl_pengaduan', 'desc')->get();

            return view('pages.masyarakat.laporan', ['pengaduan' => $pengaduan, 'hitung' => $hitung, 'who' => $who]);
        }
    }

    public function detailPengaduan($id_pengaduan)
    {
        $pengaduan = Pengaduan::where('id_pengaduan', $id_pengaduan)->first();
        $tanggapan = Tanggapan::where('id_pengaduan', $id_pengaduan)->first();
        
        if($tanggapan != null){
            $tanggapan->uploads = Upload::where("upload_id", $tanggapan->upload_id)->get();
        }

        //Ini nyambung ke relationship, otomatis dia nyari id padahal kita pakenya upload_id yg string
        // dd($tanggapan);
        return view('pages.masyarakat.detail', ['pengaduan' => $pengaduan, 'tanggapan' => $tanggapan]);
    }

    public function laporanEdit($id_pengaduan)
    {
        $pengaduan = Pengaduan::where('id_pengaduan', $id_pengaduan)->first();

        return view('user.edit', ['pengaduan' => $pengaduan]);
    }

    public function laporanUpdate(Request $request, $id_pengaduan)
    {
        $data = $request->all();

        $validate = Validator::make($data, [
            'judul_laporan' => ['required'],
            'isi_laporan' => ['required'],
            'tgl_kejadian' => ['required'],
            'lokasi_kejadian' => ['required'],
            'id_kategori' => ['required'],
        ]);

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }

        if ($request->file('foto')) {
            $data['foto'] = $request->file('foto')->store('assets/pengaduan', 'public');
        }

        $pengaduan = Pengaduan::where('id_pengaduan', $id_pengaduan)->first();

        $pengaduan->update([
            'judul_laporan' => $data['judul_laporan'],
            'isi_laporan' => $data['isi_laporan'],
            'tgl_kejadian' => $data['tgl_kejadian'],
            'lokasi_kejadian' => $data['lokasi_kejadian'],
            'id_kategori' => $data['kategori_kejadian'],
            'foto' => $data['foto'] ?? $pengaduan->foto
        ]);

        return redirect()->route('pekat.detail', $id_pengaduan);
    }

    public function laporanDestroy(Request $request)
    {
        $pengaduan = Pengaduan::where('id_pengaduan', $request->id_pengaduan)->first();

        $pengaduan->delete();

        return 'success';
    }


    public function password()
    {
        return view('user.password');
    }

    public function updatePassword(Request $request)
    {
        $data = $request->all();

        if (Auth::user()->password == null) {
            $validate = Validator::make($data, [
                'password' => ['required', 'min:6', 'confirmed'],
            ]);
        } else {
            $validate = Validator::make($data, [
                'old_password' => ['required', 'min:6'],
                'password' => ['required', 'min:6', 'confirmed'],
            ]);
        }

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate);
        }

        $id = Auth::user()->id;

        $masyarakat = Masyarakat::where('id', $id)->first();

        if (Auth::user()->password == null) {
            $masyarakat->password = Hash::make($data['password']);
            $masyarakat->save();

            return redirect()->back()->with(['pesan' => 'Password berhasil diubah!', 'type' => 'success']);
        } elseif (Hash::check($data['old_password'], $masyarakat->password)) {

            $masyarakat->password = Hash::make($data['password']);
            $masyarakat->save();

            return redirect()->back()->with(['pesan' => 'Password berhasil diubah!', 'type' => 'success']);
        } else {
            return redirect()->back()->with(['pesan' => 'Password lama salah!', 'type' => 'error']);
        }
    }

    public function ubah(Request $request, $what)
    {
        if ($what == 'email') {
            $masyarakat = Masyarakat::where('id', $request->id)->first();

            $masyarakat->email = $request->email;
            $masyarakat->save();

            return 'success';
        } elseif ($what == 'telp') {

            $validate = Validator::make($request->all(), [
                'telp' => ['required', 'regex:/(08)[0-9]/'],
            ]);

            if ($validate->fails()) {
                return 'error';
            }

            $masyarakat = Masyarakat::where('id', $request->id)->first();

            $masyarakat->telp = $request->telp;
            $masyarakat->save();

            return 'success';
        }
    }

    public function profil()
    {
        $id = Auth::user()->id;

        $masyarakat = Masyarakat::where('id', $id)->first();

        return view('user.profil', ['masyarakat' => $masyarakat]);
    }

    public function updateProfil(Request $request)
    {
        $id = Auth::user()->id;

        $data = $request->all();

        $validate = Validator::make($data, [
            // 'id' => ['sometimes', 'required', 'min:16', 'max:16', Rule::unique('masyarakat')->ignore($id, 'id')],
            'nama' => ['required', 'string'],
            // 'email' => ['sometimes', 'required', 'email', 'string', Rule::unique('masyarakat')->ignore($id, 'id')],
            'username' => ['sometimes', 'required', 'string', 'regex:/^\S*$/u', Rule::unique('masyarakat')->ignore($id, 'id'), 'unique:petugas,username'],
            'telp' => ['required', 'regex:/(08)[0-9]/'],
            'address' => ['required']
        ]);

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate);
        }

        $masyarakat = Masyarakat::where('id', $id);

        $masyarakat->update([
            'id' => $data['id'],
            'nama' => $data['nama'],
            'email' => $data['email'],
            'username' => strtolower($data['username']),
            'telp' => $data['telp'],
            'address' => $data['address'],
        ]);

        return redirect()->back()->with(['pesan' => 'Profil berhasil diubah!', 'type' => 'success']);
    }
}
