<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Masyarakat extends Authenticatable
{
    use HasFactory, SoftDeletes;

    protected $table = 'masyarakat';

    protected $primaryKey = 'id';
    public $incrementing = false;

    protected $fillable = [
        'id',
        'name',
        'telp',
        'address',
        'photo',
        'user_id'
    ];

    public function user(){
        return $this->belongsTo(User::class);

    }
}


