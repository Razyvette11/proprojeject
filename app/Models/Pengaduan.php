<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengaduan extends Model
{
    use HasFactory;

    protected $table = 'pengaduan';

    protected $primaryKey = 'id_pengaduan';

    protected $fillable = [
        'tgl_pengaduan',
        'judul_laporan',
        'isi_laporan',
        'tgl_kejadian',
        'lokasi_kejadian',
        'id_kategori',
        'foto',
        'status',
        'aduan_id',
        'user_id'
    ];

    public function masyarakat() {
        return $this->hasOne(Masyarakat::class, 'id', 'id');
    }

    public function kategori() {
        return $this->hasOne(Kategori::class, 'id_kategori', 'id_kategori');
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
