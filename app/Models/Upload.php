<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'photo',
        'upload_id' 
    ];

    public function tanggapan(){
        return $this->belongsToMany(Tanggapan::class);
    }
}
