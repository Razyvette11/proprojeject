<?php

namespace Database\Seeders;

use App\Models\Kategori;
use FontLib\Table\Type\name;
use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Models\Petugas;
use App\Models\Masyarakat;
use App\Models\RoleUser;
use App\Models\Role;
use App\Models\Pengaduan;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $masyarakat = Role::create([
            'role_name' => 'Masyarakat'
        ]);
        
        $roleadmin = Role::create([
            'role_name' => 'Administrator'
        ]);
        
        $rolepetugas = Role::create([
            'role_name' => 'Petugas'
        ]);
        
        $user = User::create([
            'name' => 'Dharma',
            'username' => 'user',
            'password' => Hash::make('password'),
            ]);
            
            RoleUser::create([
                'user_id' => $user->id,
                'role_id' => $masyarakat->id
            ]);
            
            Masyarakat::Create([
                'name' => 'Dharma',
                'telp' => '081213765223',
                'address' => 'asd',
                'user_id' => $user->id,
                ]);

            $admin = User::create([
                'name' => 'Administrator',
                'username' => 'admin',
                'password' => Hash::make('password'),
                ]);
            
                RoleUser::create([
                    'user_id' => $admin->id,
                    'role_id' => $roleadmin->id
                ]);
            
            $petugas = User::create([
            'name' => 'Petugas',
            'username' => 'petugas',
            'password' => Hash::make('password'),
            ]);
        
            RoleUser::create([
                'user_id' => $petugas->id,
                'role_id' => $rolepetugas->id
            ]);


        Kategori::create([
            'nama_kategori' => 'Hal lain'
        ]);

        Pengaduan::create([
            'tgl_pengaduan' => '2022-06-28 00:00:00',
            'judul_laporan' => 'apaya',
            'isi_laporan' => 'haha',
            'tgl_kejadian' => '2022-06-28 00:00:00',
            'lokasi_kejadian' => 'kasl',
            'id_kategori' => '1',
            'foto' => 'assets/img/hero.jpg',
            'status' => 'pending',
            'user_id' => '1'
        ]);

    }
}
