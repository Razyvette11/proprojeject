<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>Login | Pengaduan</title>

  @stack('prepend-style')
  @include('includes.admin.style')
  @stack('addon-style')
</head>

<body class="bg-white">

  <div class="container pt-7">
    <div class="row">
      <div class="col-md-6">
        <img src="assets/img/undraw_remotely_2j6y.svg" alt="Image" class="img-fluid px-3">
      </div>
      <div class="col-md-6 justify-content-center d-flex pt-5">
        <div class="pl-4 pr-5">
          <div class="mb-4">
          <h3>Log In</h3>
          <p>Silahkan login menggunakan akun yang sudah didaftarkan.</p>
        </div>
        <form method="POST" action="{{ route('login') }}">
              @csrf
            <div class="form-group mb-3">
              <div class="form-group first">
                <div class="input-group-prepend">
                  <label for="username" style="font-size: .856rem;">Username</label>
                </div>
                <input type="text" value="{{ old('username') }}" class="form-control @error('username') is-invalid @enderror" name="username" id="username" placeholder="Email atau Username">
                @error('username')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
              </div>
            </div>
            <div class="form-group mb-5">
                <div class="input-group-prepend">
                  <label for="password" style="font-size: .856rem;">Password</label>
                </div>
                <input class="form-control form-control @error('password') is-invalid @enderror" name="password" id="password" placeholder="Password" type="password">
                @error('password')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            
              <button class="btn btn-block text-white" style="background-color: #454696;" type="submit">Login</button>
            
              <div class="col text-right p-0 mt-3">
                <a href="{{ url('register')}}" class="text-primary"><small>Buat akun baru</small></a>
            </div>
          </form> 
        </div>
        
      </div>
      
    </div>
  </div>

  <!-- Footer -->
  {{-- <footer class="py-5" id="footer-main">
    <div class="container">

          <div class="copyright text-center text-muted">
            &copy; Copyright <strong><span><a href="https://madfariz.web.id/" target="_blank">MadFariz</a></span></strong>. All Rights Reserved
          </div>
    </div>
  </footer> --}}

  
  <!-- Argon Scripts -->
  @stack('prepend-script')
  @include('includes.admin.script')
  @stack('addon-script')
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  @if (session()->has('pesan'))
        <script>
            Swal.fire(
                'Pemberitahuan!',
                '{{ session()->get('pesan') }}',
                'error'
            );
        </script>
    @endif
</body>

</html>