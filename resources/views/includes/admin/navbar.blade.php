 <!-- Topnav -->
 <div class="modal fade" id="modalEditProfile{{ Auth::user()->id }}" tabindex="-1"
  aria-labelledby="modalUpdateBarang" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content" style="border-radius: 1.25rem;">
          <div class="modal-header border-0 px-4">
              <h5 class="modal-title">Edit Profile</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              <!--FORM UPDATE photo-->
              <form action="{{  Auth::user()->role[0]->role_name == "Petugas" ? route('petugas.edit', Auth::user()->id) : route('edit', Auth::user()->id) }}" method="POST"
                  enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                  <div class="row">
                    <div class="col-4">
                      {{-- @if (file_exists(asset('storage/assets/pp/' . Auth::user()->name.'.png'))) --}}
                          <img class="img-profile rounded-circle "
                              src="{{ \Auth::user()->photo ? Storage::url(\Auth::user()->photo) :  asset('assets/img/default.svg')}}" width=100% height=80%>
                      {{-- @else --}}
                          {{-- <img class="img-profile rounded-circle "
                              src="{{ asset('assets/img/default.svg') }}" width=100%> --}}
                      {{-- @endif --}}
                  </div>
                      <div class="col-8">
                          <div class="form-row">
                              <div class="form-group col-6">
                                  <label for="#" class="font-weight-bold h4">Name</label>
                                  <input type="text" class="form-control @error('name') is-invalid @enderror"
                                      id="name" name="name" id="email" placeholder="Name"
                                      value={{ Auth::user()->name }}>
                                  @error('name')
                                      <div class="invalid-feedback">
                                          {{ $message }}
                                      </div>
                                  @enderror
                              </div>
                              <div class="form-group col-6">
                                  <label for="#" class="font-weight-bold h4">Username</label>
                                  <input type="text" class="form-control" name="username" id="email"
                                      placeholder="Username" value={{ Auth::user()->username }}>
                              </div>
                          </div>
                          <label for="#" class="font-weight-bold h4 mt-3">Choose your profile picture</label>
                          <input type="hidden" name="oldImage" value={{ Auth::user()->photo }}>
                          <div class="form-group">
                              <input type="file" class="form-control" name="photo">
                          </div>
                      </div>
                  </div>
                  {{-- @if (\Auth::user()->img)
                      <img src="{{ asset('storage/bukti/' . \Auth::user()->img) }}" alt="" width="70%"
                          style="display:flex;margin-left: auto; margin-right:auto">
                  @else
                      <h4 class="text-center my-5">No Image Yet</h4>
                  @endif --}}
                  {{-- <div class="form-row">
                      <div class="form-group col-md-6">
                          <label for="#" class="font-weight-bold h6">Name</label>
                          <input type="text" class="form-control @error('name') is-invalid @enderror" id="name"
                              name="name" id="email" placeholder="Name" value={{ \Auth::user()->name }}>
                          @error('name')
                              <div class="invalid-feedback">
                                  {{ $message }}
                              </div>
                          @enderror
                      </div>
                      <div class="form-group col-md-6">
                          <label for="#" class="font-weight-bold h6">Username</label>
                          <input type="text" class="form-control" name="username" id="email"
                              placeholder="Username" value={{ \Auth::user()->username }}>
                      </div>
                  </div>
                  <label for="#" class="font-weight-bold h6 mt-3">Bukti</label>
                  <div class="form-group">
                      <input type="file" class="form-control" name="upload_bukti">
                  </div> --}}
                  <div class="modal-footer border-0">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                      <button type="submit" class="btn bt-sm right text-white" style="background-color: #454696">Save Change</button>
                  </div>
              </form>
          </div>
      </div>
  </div>
</div>
 
<nav class="main-header navbar navbar-expand px-4 py-2" style="background-color: #454696">
      <div class="container-fluid"> 
        <div class="col-6">
          <ul class="navbar-nav">
            <li class="nav-item">
              <!-- Sidenav toggler -->
              <a class="nav-link pl-0" data-widget="pushmenu" href="#" role="button">
                  <i class="fas fa-bars text-white"></i>
                </a>
              </div>
            </li> 
          </ul>
        </div>
        <div class="col-6 p-0">
          <div class="collapse navbar-collapse d-flex flex-row-reverse" id="navbarSupportedContent">
            <!-- Search form -->
            {{-- <ul class="navbar-nav ">
              <li class="nav-item">
                <div class="input-group rounded ">
                  <form id="searchForm" class="form-inline mb-0 mt-1">
                    <input class="form-control" type="search" id="searchInput" placeholder="search" style="height: 30px">
                    <button class="btn p-2"><i class="fas fa-search" style="color:#6056C0"></i></button>
                  </form>
                  </span>
                </div>
              </li>
            </ul> --}}
            
            <!-- Navbar links -->
              {{-- <li class="nav-item d-sm-none">
                <a class="nav-link" href="#" data-action="search-show" data-target="#navbar-search-main">
                  <i class="ni ni-zoom-split-in"></i>
                </a>
              </li> --}}
            
            <ul class="navbar-nav ml-auto ml-md-0">
              <li class="nav-item dropdown">
                <div class="d-flex flex-row-reverse">
                <div class="nav-link p-0 d-flex justify-content-center" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="avatar avatar-sm rounded-circle px-0 h-95">
                      <img class="img-fluid w-100 m-0 p-0" style="height: 100%;" src="{{ \Auth::user()->photo ? Storage::url(\Auth::user()->photo) :  asset('assets/img/default.svg')}}" alt="">
                    {{-- heighnya di sesuaikan sama divnya jadi biar full di jadiin 95% wkwkwk --}}
                    </div>
                    <span class="mb-0 text-md font-weight-bold text-white py-2"> &nbsp;{{ Auth::user()->name }}</span>
                </div>
                
            <div class="dropdown-menu  dropdown-menu-right">
              <div class="dropdown-header noti-title">
                <h6 class="text-overflow m-0">Welcome!</h6>
              </div>
              <a class="dropdown-item" style="font-size" data-toggle="modal"
              data-target="#modalEditProfile{{ Auth::user()->id }}">
              <i class="ni ni-single-02" style="color: #454696"></i>&nbsp;
              {{ __('Edit Profile') }}
          </a>
                {{-- <i class="ni ni-single-02"></i>
                <span>Edit profile</span>
              </a> --}}
              
             {{-- <a href="#!" class="dropdown-item">
                <i class="ni ni-settings-gear-65"></i>
                <span>Settings</span>
              </a>
              <a href="#!" class="dropdown-item">
                <i class="ni ni-calendar-grid-58"></i>
                <span>Activity</span>
              </a>
              <a href="#!" class="dropdown-item">
                <i class="ni ni-support-16"></i>
                <span>Support</span>
              </a> --}}
              
              <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
                <i class="ni ni-user-run" style="color: #454696"></i>
                <span>Logout</span>
              </a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
            </div>
          </li>
        </ul>
      </div>
</nav>
