<!-- Sidenav -->
<!-- Main Sidebar Container -->
<aside class="main-sidebar elevation-4 sidebar-light-navy">
  <!-- Brand Logo -->
  {{-- <a class="brand-link text-center" href="{{ Auth::user()->role[0]->role_name == "Petugas" ? route('petugas.home') : route('admin.home')}}">
        <h2 class="text-uppercase" style="color: #454696">Pengaduan</h2>
  </a> --}}

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel py-3 d-flex">
      <a class="d-block" href="{{ Auth::user()->role[0]->role_name == "Petugas" ? route('petugas.home') : route('admin.home')}}">
      <div class="image pt-1">
        <i class="fas fa-tv" style="color: #454696"></i>
      </div>
      <div class="info">
              Dashboard   
      </a>
      </div>
    </div>

    <!-- SidebarSearch Form -->

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <!-- <li class="nav-item menu-open">
        <a class="nav-link {{ (request()->segment(2) == 'pengaduan') ? 'active' : '' }}" href="#">
        <i class="fas fa-bullhorn text-danger"></i>
            <p>
            Pengaduan
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
            <a class="nav-link" href="{{ route('pengaduan.index','pending')}}">
            <i class="fas fa-list text-primary"></i>
                  <span class="nav-link-text"> Verifikasi & Validasi</span>
              </a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="{{ route('pengaduan.index','proses')}}">
                    <i class="fas fa-list text-primary"></i>
                    <span class="nav-link-text"> Proses</span>
              </a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="{{ route('pengaduan.index', 'selesai')}}">
                      <i class="fas fa-list text-primary"></i>
                      <span class="nav-link-text">Selesai</span>
              </a>
            </li>
          </ul>
        </li> -->
        <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#collapseExempel" role="button" aria-expanded="false" aria-controls="collapse">
        <i class="fas fa-bullhorn pr-2" style="color: red"></i>
            <p>
              Pengaduan
              <i class="fas fa-angle-down right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a class="nav-link {{ (request()->segment(2) == 'pengaduan/0') ? 'active' : '' }}" href="{{  Auth::user()->role[0]->role_name == "Petugas" ? route('petugas.pengaduan.index','pending') : route('pengaduan.index', 'pending')}}">
                    <i class="fas fa-clipboard-check pr-2" style="color: lightblue"></i> 
                    <p> Verifikasi & Validasi </p>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{  Auth::user()->role[0]->role_name == "Petugas" ? route('petugas.pengaduan.index','proses') : route('pengaduan.index','proses')}}">
                  <i class="fas fa-sync pr-2" style="color: lightsalmon"></i> 
                    <p>Proses</p>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ Auth::user()->role[0]->role_name == "Petugas" ? route('petugas.pengaduan.index','selesai') : route('pengaduan.index', 'selesai')}}">
                    <i class="fas fa-check pr-2" style="color: lightgreen  "></i> 
                    <p>Selesai</p> 
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ Auth::user()->role[0]->role_name == "Petugas" ? route('petugas.kategori.index') : route('kategori.index')}}">
                <i class="fas fa-list text-primary pr-2"></i>
                <p>
                  Kategori Pengaduan 
                </p>
          </a>  
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ Auth::user()->role[0]->role_name == "Petugas" ? route('petugas.laporan.index') : route('laporan.index')}}">
                <i class="fas fa-file-alt text-green pr-2"></i>
                <p>
                  Laporan 
                </p>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ Auth::user()->role[0]->role_name == "Petugas" ? route('petugas.masyarakat.index') : route('masyarakat.index')}}">
                <i class="fas fa-users text-default pr-2"></i>
                <p>
                  Masyarakat
                </p>
          </a>
          </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ Auth::user()->role[0]->role_name == "Petugas" ? route('petugas.petugas.index') : route('admin.petugas.index')}} ">
              <i class="fas fa-users-cog pr-2" style="color: slateblue"></i>
              <p>
                Petugas 
              </p>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
