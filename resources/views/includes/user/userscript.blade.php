<!-- Argon Scripts -->
  <!-- Core -->
  <script src="{{ asset('assets/backend/vendor/jquery/dist/jquery.min.js')}} "></script>
  <script src="{{ asset('assets/backend/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{ asset('assets/backend/vendor/js-cookie/js.cookie.js')}} "></script>
  <script src="{{ asset('assets/backend/vendor/jquery.scrollbar/jquery.scrollbar.min.js')}} "></script>
  <script src="{{ asset('assets/backend/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js')}} "></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <!-- Optional JS -->
  <script src="{{ asset('assets/backend/vendor/chart.js/dist/Chart.min.js')}} "></script>
  <script src="{{ asset('assets/backend/vendor/chart.js/dist/Chart.extension.js')}} "></script>
  <!-- Argon JS -->
  <script src="{{ asset('assets/backend/js/argon.js?v=1.2.0')}} "></script>

  <!-- jQuery UI 1.11.4 -->
<script src="{{ asset('js/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- JQVMap -->
<script src="{{ asset('js/jquery.vmap.min.js')}}"></script>
<script src="{{ asset('js/maps/jquery.vmap.usa.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('js/adminlte.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('js/demo.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('js/dashboard.js')}}"></script>