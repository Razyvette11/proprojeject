<!-- Sidenav -->
<!-- Main Sidebar Container -->
<aside class="main-sidebar elevation-4 sidebar-light-navy">
  <!-- Brand Logo -->
  {{-- <a class="brand-link text-center" href="{{ route('masyarakat.home')}}">
        <h2 class="text-uppercase" style="color: #454696">Pengaduan</h2>
  </a> --}}

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel py-3 d-flex">
      <a class="d-block {{ (request()->segment(2) == 'dashboard') ? 'active' : '' }}" href="{{ route('masyarakat.home')}}">
      <div class="image pt-1">
        <i class="fas fa-tv" style="color: #454696"></i>
      </div>
      <div class="info">
              Dashboard   
      </a>
      </div>
    </div>

    <!-- SidebarSearch Form -->

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item">
          <a class="nav-link" href="{{ route('masyarakat.pengaduan')}}">
            <i class="fas fa-file-alt text-green pr-1"></i>
            <p>Form Aduan</p>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ (request()->is('pengaduan.laporan')) ? 'active' : '' }}" href="{{ route('masyarakat.pengaduan.laporan', 'saya')}}">
            <i class="fas fa-users text-default pr-1"></i>
            <p>Aduan Saya</p>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>