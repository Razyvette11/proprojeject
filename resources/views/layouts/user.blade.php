<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>@yield('title') | Pengaduan</title>

  @stack('prepend-style')
  @include('includes.user.userstyle')
  @stack('addon-style')

</head>

<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">
    <!-- Sidenav -->
    @include('includes.user.usersidebar')
    <!-- Main content -->

      <!-- Topnav -->
      @include('includes.user.usernavbar')
  </div> 
    
  <div class="content-wrapper">
    @yield('content')

    @include('includes.user.userfooter')
  </div>
  @stack('prepend-script')
  @include('includes.user.userscript')
  @stack('addon-script')
  @include('sweetalert::alert')
</body>

</html>
