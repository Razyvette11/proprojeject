@extends('layouts.admin')
@section('title', 'Dashboard')

@section('content')
    <!-- Header -->
<div class="header">
    <div class="container-fluid p-4 py-4">
            <h2 class=" d-inline-block mb-4">Dashboard</h2>
            {{-- <div class="header-body">
            <div class="row align-items-center py-4">
            <div class="col-12">
            <div class="header">
                <div class="container-fluid mt-4">
                  <h6 class="h2 d-inline-block mb-4">Dashboard</h6>
                   <div class="header-body">
                     <div class="row align-items-center py-4">
                      <div class="col-12">
                        <h6 class="h2 d-inline-block mb-0">Dashboard</h6>
                        <hr class="mt-3 mb-0">
                      </div> 
                <div class="col-lg-6 col-5 text-right">
                            <a href="#" class="btn btn-sm btn-neutral">New</a>
                            <a href="#" class="btn btn-sm btn-neutral">Filters</a>
                          </div>
                     </div>
                   </div> 
                 </div></div></div></div> </div>  --}}
            <!-- Card stats -->
            <div class="row">
              <div class="col-12 col-lg-6 col-xl">
                  <!-- Value  -->
                  <div class="card" style="border-radius: 1.5rem; box-shadow: 0 .1rem 1rem rgb(23,43,77,.14)">
                      <div class="card-body p-3">
                          <div class="row align-items-center gx-0">
                              <div class="col">
                                  <!-- Title -->
                                  <h6 class="text-uppercase text-muted mb-0">Semua Pengaduan</h6>
                                  <!-- Heading -->
                                  <span class="h2 mb-0">{{ $pengaduan }}</span>
                              </div>
                              <div class="col-auto">
                                  <!-- Icon -->
                                  <i class="fas fa-bullhorn" style="color: red; font-size: 20px;"></i>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>

              <div class="col-12 col-lg-6 col-xl">
                  <!-- Value  -->
                  <div class="card" style="border-radius: 1.5rem; box-shadow: 0 .1rem 1rem rgb(23,43,77,.14)">
                      <div class="card-body p-3">
                          <div class="row align-items-center gx-0">
                              <div class="col">
                                  <!-- Title -->
                                  <h6 class="text-uppercase text-muted mb-0">Diproses</h6>
                                  <!-- Heading -->
                                  <span class="h2 mb-0">{{ $proses }}</span>
                              </div>
                              <div class="col-auto">
                                  <!-- Icon -->
                                  <i class="fas fa-sync" style="color: coral; font-size: 20px;"></i>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>

              <div class="col-12 col-lg-6 col-xl">
                  <!-- Value  -->
                  <div class="card" style="border-radius: 1.5rem; box-shadow: 0 .1rem 1rem rgb(23,43,77,.14)">
                      <div class="card-body p-3">
                          <div class="row align-items-center gx-0">
                              <div class="col">
                                  <!-- Title -->
                                  <h6 class="text-uppercase text-muted mb-0">Selesai</h6>
                                  <!-- Heading -->
                                  <span class="h2 mb-0">{{ $selesai }}</span>
                              </div>
                              <div class="col-auto">
                                  <!-- Icon -->
                                  <i class="fas fa-check-circle" style="color: #2dce89; font-size: 20px;"></i>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>

              <div class="col-12 col-lg-6 col-xl">
                  <!-- Value  -->
                  <div class="card" style="border-radius: 1.5rem; box-shadow: 0 .1rem 1rem rgb(23,43,77,.14)">
                      <div class="card-body p-3">
                          <div class="row align-items-center gx-0">
                              <div class="col">
                                  <h6 class="text-uppercase text-muted mb-0">Total Masyarakat</h6>
                                  <span class="h2 mb-0">{{ $masyarakat }}</span>
                              </div>
                              <div class="col-auto">
                                <i class="fas fa-users" style="color: #172b4d; font-size: 20px;"></i>                              
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              
                  <div class="col-12">
                    <div class="card" style="border-radius: 1.5rem; box-shadow: 0 .1rem 1rem rgb(23,43,77,.14)">
                      <div class="card-header border-0 pt-4" style="border-radius: 1.5rem">
                        <h4 class="card-header-title">Statistik Pengaduan</h4>
                      </div> 

                      <div class="card-body pt-3 pb-3">
                        <canvas id="myChart"></canvas>
                        {{-- <div class="chart pb-2">
                          {{--chartnya jangan di masukin div karena di masukin div jadi kebates sama si div ini , jadi sizeenya menttok  --}}
                        {{-- </div> --}}
                      </div>
                    </div>
                  </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        const labels = [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Agust',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        ];

        const data = {
            labels: labels,
            datasets: [{
                label: 'Data Aduan Yang masuk',
                backgroundColor: '#454696',
                // borderColor: 'rgb(255, 99, 132)',
                borderRadius: 20,
                // borderSkipped: false,
                barThickness: 10,
                backgroundColor: '#9286E7',
                borderRadius: 20,
                // borderSkipped: true,
                barThickness: 10,
                data: [
                  @foreach ($data_month_all as $da)
                  {{ $da }},
                  @endforeach
                ]
            }, {
                label: 'Data Aduan Selesai',
                backgroundColor: '#454696',
                // borderColor: 'rgb(255, 99, 132)',
                borderRadius: 20,
                // borderSkipped: false,
                barThickness: 10,
                data: [
                  @foreach ($data_month_done as $dd)
                  {{ $dd }},
                  @endforeach
                ],
            }]
        };

        const config = {
            type: 'bar',
            data: data,
            options: {}
        };
    </script>

    <script>
        const myChart = new Chart(
            document.getElementById('myChart'),
            config
        );
    </script>



@endsection
