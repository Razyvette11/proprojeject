@extends('layouts.admin')
@section('title', 'Tambah Kategori')


@push('addon-style')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
@endpush
@section('content')
    <!-- Header -->
    
      <div class="container-fluid p-4 py-4">
              <h2 class=" d-inline-block mb-4">Tambah Kategori</h2>
              {{-- <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Petugas</a></li>
                </ol>
              </nav> --}}
        <!-- Page content -->
        <div class="container-fluid">
            <div class="row">
              <div class="col-xl-6">
                <div class="card" style="border-radius: 1.25rem; box-shadow: 0 .1rem 1rem rgb(23,43,77,.14)">
                  <div class="card-body">
                    <form action="{{ route('kategori.store')}} " method="POST">
                    @csrf
                      <!-- Kategori -->
                      <div class="">
                        <div class="form-group">
                          <label class="form-control-label">Nama Kategori</label>
                          <input rows="4" class="form-control" name="nama_kategori" id="nama_kategori" placeholder="Ketik Nama Kategori" required>
                        </div>
                      </div>
                      <button type="submit" class="btn text-white" style="background-color: #454696;">Simpan</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
@endsection

@push('addon-script')
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#pengaduanTable').DataTable();
    } );
</script>
@if (session()->has('status'))
<script>
    Swal.fire({
        title: 'Pemberitahuan!',
        text: "{{ Session::get('status') }}",
        icon: 'success',
        confirmButtonColor: '#454696',
        confirmButtonText: 'OK',
    });
    </script>
@endif
@endpush
