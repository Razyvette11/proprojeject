@extends('layouts.admin')
@section('title', 'Kategori')


@push('addon-style')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
@endpush
@section('content')
    <!-- Header -->
    
      <div class="container-fluid p-4 py-4">
          <h2 class=" d-inline-block mb-4">Kategori</h2>
              {{-- <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Petugas</a></li>
                </ol>
              </nav> --}}          
          <div class="row">
            <div class="col">
              <div class="card" style="border-radius: 1.5rem; box-shadow: 0 .1rem 1rem rgb(23,43,77,.14)">
                <!-- Card header -->
                <div class="card-header border-0 d-flex justify-content-between p-3 mr-2" style="border-radius: 1.5rem">
                  <h3 class="mb-0 d-flex align-items-center">Data Kategori</h3>
                  @if (Auth::user()->role[0]->role_name != 'Petugas')
                  <a href="{{ route('kategori.create')}}" class="btn text-white p-2" style="background-color: #454696;">
                    <i class="fas fa-plus"></i> Tambah Kategori</a>
                  @endif
                </div>
                <div class="card-body pt-0">
                <!-- Light table -->
                  <div class="table-responsive">
                    <table class="table align-items-center table-flush" id="pengaduanTable">
                      <thead class="thead-light">
                        <tr>
                          <th scope="col" class="sort" data-sort="no">No</th>
                          <th scope="col" class="sort" data-sort="name">Nama Kategori</th>
                          @if (Auth::user()->role[0]->role_name == 'Petugas')
                          <th scope="col" class="none" data-sort="none" style="text-align:center";>Aksi <br>(Hanya dapat diakses oleh admin)</th>
                          {{-- <th scope="col">Aksi (Hanya dapat diakses oleh Admin)</th> --}}
                          @elseif (Auth::user()->role[0]->role_name != 'Petugas')
                          <th scope="col" class="sort" data-sort="action">Aksi</th>
                          @endif
                        </tr>
                      </thead>
                      <tbody class="list">
                          @foreach($kategori as $k => $kat)

                          <tr>
                            <td class="budget">
                                <span class="text-sm">{{ $k += 1}}</span>
                            </td>
                            <td><span class="text-sm">{{ $kat->nama_kategori}}</span></td>
                            <td style="width: 100px;">
                                @if (Auth::user()->role[0]->role_name != 'Petugas')
                                <a href="{{ Auth::user()->role[0]->role_name == "Petugas" ? route('petugas.kategori.edit', $kat->id_kategori) : route('kategori.edit', $kat->id_kategori)}}" class="btn btn-sm text-white" style="background-color: #454696;"><i class="fas fa-pen"></i> Edit</a>
                                <a href="#" data-id_kategori="{{ $kat->id_kategori }}" class="btn btn-sm btn-danger kategoriDelete"><i class="fas fa-trash"></i> Hapus</a>
                                @endif
                            </td>
                          </tr>

                          @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- Card footer -->
                {{-- <div class="card-footer py-4">
                  <nav aria-label="...">
                    <ul class="pagination justify-content-end mb-0">
                      <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1">
                          <i class="fas fa-angle-left"></i>
                          <span class="sr-only">Previous</span>
                        </a>
                      </li>
                      <li class="page-item active">
                        <a class="page-link" href="#">1</a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                      </li>
                      <li class="page-item"><a class="page-link" href="#">3</a></li>
                      <li class="page-item">
                        <a class="page-link" href="#">
                          <i class="fas fa-angle-right"></i>
                          <span class="sr-only">Next</span>
                        </a>
                      </li>
                    </ul>
                  </nav>
                </div> --}}
              </div>
            </div>
          </div>
        
      </div>
@endsection

@push('addon-script')
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function() {
        $('#pengaduanTable').DataTable();
    } );
</script>

<script>

    $(document).on('click', '#del', function(e) {
        let id = $(this).data('userId');
        console.log(id);
    });

    $(document).on('click', '.kategoriDelete', function (e) {
        e.preventDefault();
        let id_kategori = $(this).data('id_kategori');
        Swal.fire({
                title: 'Peringatan!',
                text: "Apakah Anda yakin akan menghapus kategori?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#454696',
                confirmButtonText: 'OK',
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "DELETE",
                    url: '{{ route('kategori.destroy', 'id_kategori') }}',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id_kategori": id_kategori,
                    },
                    success: function (response) {
                        if (response == 'success') {
                            Swal.fire({
                                title: 'Pemberitahuan!',
                                text: "Kategori berhasil dihapus!",
                                icon: 'success',
                                confirmButtonColor: '#454696',
                                confirmButtonText: 'OK',
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload();
                                }else{
                                    location.reload();
                                }
                            });
                        }
                    },
                    error: function (data) {
                        Swal.fire({
                            title: 'Pemberitahuan!',
                            text: "Kategori gagal dihapus!",
                            icon: 'error',
                            confirmButtonColor: '#454696',
                            confirmButtonText: 'OK',
                        });
                    }
                });
            } else {
                Swal.fire({
                    title: 'Pemberitahuan!',
                    text: "Kategori gagal dihapus!",
                    icon: 'error',
                    confirmButtonColor: '#454696',
                    confirmButtonText: 'OK',
                });
            }
        });
    });



</script>
@endpush
