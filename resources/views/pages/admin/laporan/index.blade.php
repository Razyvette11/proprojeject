@extends('layouts.admin')
@section('title', 'Laporan')


@push('addon-style')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
@endpush
@section('content')
    <!-- Header -->
    
        <div class="container-fluid p-4 py-4">
          <h2 class="d-inline-block mb-4">Laporan</h2>
                {{-- <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                  <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                    <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#">Petugas</a></li>
                  </ol>
                </nav> --}}
                <div class="row">
                  <div class="col">
                    <div class="row">
                      <div class="col-xl-3 order-xl-1">
                        <div class="card" style="border-radius: 1.25rem; box-shadow: 0 .1rem 1rem rgb(23,43,77,.14)">
                          <div class="card-header border-0 p-3" style="border-radius: 1.25rem;">
                            <h3 class="mb-0 d-flex align-items-center">Filter Laporan</h3>
                          </div>
                          <div class="card-body pt-0">
                            <form action="{{ Auth::user()->role[0]->role_name == "Petugas" ? route('petugas.laporan.get') : route('laporan.get') }}" method="POST">
                               @csrf
                                <div class="form-group">
                                    <input type="text" name="date_from" class="form-control" placeholder="Tanggal Awal"
                                        onfocusin="(this.type='date')" onfocusout="(this.type='text')" value="{{ $from ?? '' }}">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="date_to" class="form-control" placeholder="Tanggal Akhir"
                                        onfocusin="(this.type='date')" onfocusout="(this.type='text')" value="{{ $to ?? '' }}">
                                </div>
                                <button type="submit" class="btn text-white mt-3" style="width: 100%; background-color: #454696">Cari Laporan</button>
                           </form>
                          </div>
                        </div>
                      </div>
                      <div class="col-xl-9 order-xl-2">
                        <div class="card" style="border-radius: 1.25rem; box-shadow: 0 .1rem 1rem rgb(23,43,77,.14)">
                          <div class="card-header border-0 p-3" style="border-radius: 1.25rem;">
                            <div class="row align-items-center">
                              <div class="col-8">
                                <h3 class="mb-0 d-flex align-items-center">Data Pengaduan</h3>
                              </div>
                              <div class="col text-right">
                                @if ($pengaduan ?? '')
                                <form action="{{ Auth::user()->role[0]->role_name == "Petugas" ? route('petugas.laporan.export') : route('laporan.export') }}" method="POST">
                                  @csrf          
                                    <input type="hidden" name="date_from" value="{{ $from }}">
                                    <input type="hidden" name="date_to" value="{{ $to }}">
                                    <button type="submit" class="btn text-white p-2 align-items-center" style="background-color: #454696;"><i class="fas fa-file-pdf"></i> Export PDF</button>
                                  </form>
                                @endif
                              </div>
                            </div>
                          </div>
                          <div class="card-body pt-0">
                            @if($pengaduan ?? '')
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Tanggal Pengaduan</th>
                                            <th>Nama</th>
                                            <th>Judul Laporan</th>
                                            <th>Isi Laporan</th>
                                            {{-- <th>Tanggal Kejadian</th> --}}
                                            {{-- <th>Lokasi Kejadian</th> --}}
                                            {{-- <th>Kategori</th> --}}
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($pengaduan ?? '' as $k => $i)
                                        <tr>
                                            <td>{{ $k += 1 }}.</td>
                                            <td>{{ Carbon\Carbon::parse($i->tgl_pengaduan)->format('d-m-Y') }}</td>
                                            <td>{{ $i->user()->withTrashed()->first()->name }}</td>
                                            <td>{{ Str::limit($i->judul_laporan, 5) }}</td>
                                            <td>{{ Str::limit($i->isi_laporan, 8) }}</td>
                                            <!-- <td>{{ Carbon\Carbon::parse($i->tgl_kejadian)->format('d-m-Y') }}</td>
                                            <td>{{ $i->lokasi_kejadian }}</td>
                                            <td>{{ $i->kategori->nama_kategori }}</td> -->
                                            <td>
                                              @if($i->status == 'pending')
                                                <span class="badge bg-danger text-white" style="font-size: 10px">Pending</span>
                                              @elseif($i->status == 'proses')
                                                <span class="badge bg-warning text-white" style="font-size: 10px">Proses</span>
                                              @else
                                                <span class="badge bg-success text-white" style="font-size: 10px">Selesai</span>
                                              @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @endif
                          </div>
                  </div>
                </div>
        </div>
      <!-- Page content -->
@endsection

@push('addon-script')
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function() {
        $('#pengaduanTable').DataTable();
    } );
</script>

<script>

    $(document).on('click', '#del', function(e) {
        let id = $(this).data('userId');
        console.log(id);
    });

    $(document).on('click', '.petugasDelete', function (e) {
        e.preventDefault();
        let id = $(this).data('id');
        let role_id = $(this).data('role_id');
        let url = "";

        if(role_id == 2){
          url = '{{ route('admin.petugas.destroy', 'id') }}'
        }
        else if(role_id == 3){
          url = '{{ route('petugas.petugas.destroy', 'id') }}';
        }
        Swal.fire({
                title: 'Peringatan!',
                text: "Apakah Anda yakin akan menghapus petugas?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#454696',
                confirmButtonText: 'OK',
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "DELETE",
                    url: url,
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id": id,
                    },
                    success: function (response) {
                        if (response == 'success') {
                            Swal.fire({
                                title: 'Pemberitahuan!',
                                text: "Petugas berhasil dihapus!",
                                icon: 'success',
                                confirmButtonColor: '#454696',
                                confirmButtonText: 'OK',
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload();
                                }else{
                                    location.reload();
                                }
                            });
                        }
                    },
                    error: function (data) {
                        Swal.fire({
                            title: 'Pemberitahuan!',
                            text: "Petugas gagal dihapus!",
                            icon: 'error',
                            confirmButtonColor: '#454696',
                            confirmButtonText: 'OK',
                        });
                    }
                });
            } else {
                Swal.fire({
                    title: 'Pemberitahuan!',
                    text: "Petugas gagal dihapus!",
                    icon: 'error',
                    confirmButtonColor: '#454696',
                    confirmButtonText: 'OK',
                });
            }
        });
    });



</script>
@endpush
