@extends('layouts.admin')
@section('title', 'Detail Masyarakat')


@push('addon-style')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
@endpush
@section('content')
    <!-- Header -->
      <div class="container-fluid p-4 py-4">
              <h2 class="d-inline-block mb-4">Detail Masyarakat</h2>
              {{-- <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Petugas</a></li>
                </ol>
              </nav> --}}
    <!-- Page content -->
 
        <div class="row">
          <div class="col-xl-5">
            <div class="card" style="box-shadow: 0 .1rem 1rem rgb(23,43,77,.14); border-radius: 1.5rem">

              <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-2" style="border-radius: 1.25rem">
                <h3>Detail Masyarakat</h3>
              </div>
              <div class="card-body pt-0">
                <table class="table-tanggapan">
                    <tbody>
                        <tr>
                            <th>Nama</th>
                            <td>:</td>
                            <td>{{ $masyarakat->name }}</td>
                        </tr>
                        <tr>
                            <th>Username</th>
                            <td>:</td>
                            <td>{{ $masyarakat->user->username }}</td>
                        </tr>
                        <tr>
                            <th>No Telpon</th>
                            <td>:</td>
                            <td>{{ $masyarakat->telp }}</td>
                        </tr>
                        <tr>
                            <th>Alamat</th>
                            <td>:</td>
                            <td>{{ $masyarakat->address }}</td>
                        </tr>
                    </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
      </div>
    
@endsection

@push('addon-script')
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#pengaduanTable').DataTable();
    } );
</script>
@if (session()->has('status'))
<script>
    Swal.fire({
        title: 'Pemberitahuan!',
        text: "{{ Session::get('status') }}",
        icon: 'success',
        confirmButtonColor: '#454696',
        confirmButtonText: 'OK',
    });
    </script>
@endif
@endpush
