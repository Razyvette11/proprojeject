@extends('layouts.admin')
@section('title', 'Pengaduan')


@push('addon-style')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
@endpush
@section('content')
    <!-- Header -->
    
      <div class="container-fluid  p-4 py-4">    
          <h6 class="h2 d-inline-block mb-4">Pengaduan</h6>
                {{-- <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                  <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                    <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#">Pengaduan</a></li>
                  </ol>
                </nav> --}}
          <!-- Page content -->
          <div class="row">
            <div class="col">
              <div class="card" style="border-radius: 1.25rem; box-shadow: 0 .1rem 1rem rgb(23,43,77,.14)">
                <!-- Card header -->
                <div class="card-header border-0" style="border-radius: 1.25rem; ">
                  <h3 class="mb-0">Data Pengaduan</h3>
                </div>
                <!-- Light table -->
                <div class="card-body pt-0" style="">
                  <div class="table-responsive">
                    <table class="table align-items-center table-flush" id="pengaduanTable">
                      <thead class="thead-light">
                        <tr>
                          <th scope="col" class="sort" data-sort="no">No</th>
                          <th scope="col" class="sort" data-sort="tanggal">Tanggal</th>
                          {{-- <th scope="col" class="sort" data-sort="name">Nama</th> --}}
                          <th scope="col" class="sort" data-sort="isi">Isi Laporan</th>
                          <th scope="col" class="sort" data-sort="status">Status</th>
                          <th scope="col" class="sort" data-sort="action">Aksi</th>
                        </tr>
                      </thead>
                      <tbody class="list">
                          @foreach($pengaduan as $k => $v)
                          @php
                              // dd($pengaduan);
                          @endphp
                          <tr>
                            <td class="budget">
                                <span class="text-sm">{{ $k += 1}}</span>
                            </td>
                            <td>
                                <span class="text-sm">{{ \Carbon\Carbon::parse($v->tgl_pengaduan)->format('d-m-Y') }}</span>
                            </td>
                            {{-- <td><span class="text-sm">{{ $v->user->name }}</span></td> --}}
                            <td>
                                <span class="text-sm">{{ Str::limit($v->isi_laporan, 30)}}</span>
                            @php
                            // dd($v);
                            @endphp
                            </td>
                            <td>
                              <div class="d-flex justify-content-center">
                                @if($v->status == 'pending')
                                    <span class="badge bg-danger text-white" style="font-size: 10px;">Pending</span>
                                @elseif($v->status == 'proses')
                                    <span class="badge bg-warning text-white" style="font-size: 10px;">Proses</span>
                                @else
                                    <span class="badge bg-success text-white" style="font-size: 10px;">Selesai</span>
                                @endif
                              </div>
                            </td>
                            @if ($status == 'pending')
                                <td class="justify-content-center d-flex">
                                  <a href="{{ Auth::user()->role[0]->role_name == "Petugas" ? route('petugas.aduan.show', $v->id_pengaduan) : route('aduan.show', $v->id_pengaduan)}}" class="btn text-white btn-sm" style="background-color: #6056C0" >Lihat</a>
                                  @if(Auth::user()->role[0]->role_name != "Petugas")
                                    <a href="#" data-id_pengaduan="{{ $v->id_pengaduan }}" data-role_id="{{ Auth::user()->role[0]->id }}" class="btn text-white btn-sm pengaduan" style="background-color: #454696;">Verifikasi</a>
                                    <a href="#" data-id_pengaduan="{{ $v->id_pengaduan }}" data-role_id="{{ Auth::user()->role[0]->id }}" class="btn btn-danger  btn-sm pengaduanDelete">Hapus</a>
                                  @endif  
                                </td>
                            @else
                                <td class="justify-content-center d-flex">
                                    <a href="{{ Auth::user()->role[0]->role_name == "Petugas" ? route('petugas.pengaduan.show', $v->id_pengaduan) : route('pengaduan.show', $v->id_pengaduan)}}" class="btn text-white btn-sm" style="background-color: #454696;">Lihat</a>
                                </td>
                            @endif
                          </tr>

                          @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- Card footer -->
                {{-- <div class="card-footer py-4">
                  <nav aria-label="...">
                    <ul class="pagination justify-content-end mb-0">
                      <li class="page-item disabled">
                         <a class="page-link" href="#" tabindex="-1">
                          <i class="fas fa-angle-left"></i>
                          <span class="sr-only">Previous</span>
                        </a>
                      </li>
                    </ul>
                  </nav>
                </div>  --}}
              </div>
            </div>
          </div>
      </div>
@endsection

@push('addon-script')
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#pengaduanTable').DataTable();
    } );
</script>

<script>
    $(document).on('click', '#del', function(e) {
        let id = $(this).data('userId');
        console.log(id);
    });

    $(document).on('click', '.pengaduan', function (e) {
        e.preventDefault();
        let id_pengaduan = $(this).data('id_pengaduan');
        let role_id = $(this).data('role_id');
        let url = "";

        if(role_id ==  2){
            url = '{{ route('tanggapan') }}'
        }
        else if(role_id == 3){
            url = '{{ route('petugas.tanggapan') }}'
        }
        Swal.fire({
                title: 'Peringatan!',
                text: "Apakah Anda yakin akan memverifikasi pengaduan?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#454696',
                confirmButtonText: 'OK',
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id_pengaduan": id_pengaduan,
                        "status": "proses",
                        "tanggapan": ''
                    },
                    success: function (response) {
                        if (response == 'success') {
                            Swal.fire({
                                title: 'Pemberitahuan!',
                                text: "Pengaduan berhasil diverifikasi!",
                                icon: 'success',
                                confirmButtonColor: '#454696',
                                confirmButtonText: 'OK',
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload();
                                }else{
                                    location.reload();
                                }
                            });
                        }
                    },
                    error: function (data) {
                        Swal.fire({
                            title: 'Pemberitahuan!',
                            text: "Pengaduan gagal diverifikasi!",
                            icon: 'error',
                            confirmButtonColor: '#454696',
                            confirmButtonText: 'OK',
                        });
                    }
                });
            } else {
                Swal.fire({
                    title: 'Pemberitahuan!',
                    text: "Pengaduan gagal diverifikasi!",
                    icon: 'error',
                    confirmButtonColor: '#454696',
                    confirmButtonText: 'OK',
                });
            }
        });
    });

    $(document).on('click', '.pengaduanDelete', function (e) {
        e.preventDefault();
        let id_pengaduan = $(this).data('id_pengaduan');
        let role_id = $(this).data('role_id');
        let url = "";

        if(role_id ==  2){
            url = '{{ route('pengaduan.delete','id_pengaduan') }}'
        }
        else if(role_id == 3){
            url = '{{ route('petugas.pengaduan.delete','id_pengaduan') }}'
        }
        Swal.fire({
                title: 'Peringatan!',
                text: "Apakah Anda yakin akan menghapus pengaduan?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#454696',
                confirmButtonText: 'OK',
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "DELETE",
                    url: url,
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id_pengaduan": id_pengaduan,
                    },
                    success: function (response) {
                        if (response == 'success') {
                            Swal.fire({
                                title: 'Pemberitahuan!',
                                text: "Pengaduan berhasil dihapus!",
                                icon: 'success',
                                confirmButtonColor: '#454696',
                                confirmButtonText: 'OK',
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload();
                                }else{
                                    location.reload();
                                }
                            });
                        }
                    },
                    error: function (data) {
                        Swal.fire({
                            title: 'Pemberitahuan!',
                            text: "Pengaduan gagal dihapus!",
                            icon: 'error',
                            confirmButtonColor: '#454696',
                            confirmButtonText: 'OK',
                        });
                    }
                });
            } else {
                Swal.fire({
                    title: 'Pemberitahuan!',
                    text: "Pengaduan gagal dihapus!",
                    icon: 'error',
                    confirmButtonColor: '#454696',
                    confirmButtonText: 'OK',
                });
            }
        });
    });



</script>
@endpush
