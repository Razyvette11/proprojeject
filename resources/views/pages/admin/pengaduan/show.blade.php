@extends('layouts.admin')
@section('title', 'Pengaduan')


@push('addon-style')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
@endpush
@section('content')
    <!-- Header -->
    
      <div class="container-fluid p-4">
              <h2 class="d-inline-block mb-4">Tanggapan</h2>
              {{-- <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Petugas</a></li>
                </ol>
              </nav> --}}
          
    
    <!-- Page content -->
   
        <div class="row">
          <div class="col-xl-6">
            <div class="card" style="border-radius: 1.5rem; box-shadow: 0 .1rem 1rem rgb(23,43,77,.14)">
              <img src="{{ Storage::url($pengaduan->foto) }} " alt="User Tidak Mengupload Foto" height="300" class="card-img-top p-3">
              <div class="card-header text-center border-0 pb-0 m-0" style="border-radius: 1.5rem">
                <h3>Data Pengaduan</h3>
              </div>
              <div class="card-body pt-2">
                  <table class="table-tanggapan">
                      <tbody>
                          <tr>
                              <th>Nama</th>
                              <td>:</td>
                              <td>{{ $pengaduan->user()->withTrashed()->first()->name }}</td>
                          </tr>
                          <tr>
                              <th>Tanggal Pengaduan</th>
                              <td>:</td>
                              <td>{{ \Carbon\Carbon::parse($pengaduan->tgl_pengaduan)->format('d-m-Y') }}</td>
                          </tr>
                          <tr>
                              <th>Judul Laporan</th>
                              <td>:</td>
                              <td>{{ $pengaduan->judul_laporan }}</td>
                          </tr>
                          <tr>
                              <th>Isi Laporan</th>
                              <td>:</td>
                              <td>{{ $pengaduan->isi_laporan }}</td>
                          </tr>
                          <tr>
                              <th>Tanggal Kejadian</th>
                              <td>:</td>
                              <td>{{ \Carbon\Carbon::parse($pengaduan->tgl_kejadian)->format('d-m-Y') }}</td>
                          </tr>
                          <tr>
                              <th>Kategori Kejadian</th>
                              <td>:</td>
                              <td>{{ ucwords($pengaduan->kategori->nama_kategori) }}</td>
                          </tr>
                          <tr>
                              <th>Status</th>
                              <td>:</td>
                              <td>
                                  @if($pengaduan->status == 'pending')
                                      <span class="badge bg-danger text-white" style="font-size: 10px">Pending</span>
                                  @elseif($pengaduan->status == 'proses')
                                      <span class="badge bg-warning text-white" style="font-size: 10px">Proses</span>
                                  @else
                                      <span class="badge bg-success text-white" style="font-size: 10px">Selesai</span>
                                  @endif
                              </td>
                          </tr>
                          <tr>
                              <th>Lokasi Kejadian</th>
                              <td>:</td>
                              <td>{{ $pengaduan->lokasi_kejadian }}</td>
                          </tr>
                      </tbody>
                  </table>
              </div>
            </div>
          </div>
          @if ($pengaduan->status != 'selesai')
          <div class="col-xl-6">
            <div class="card" style="border-radius: 1.5rem; box-shadow: 0 .1rem 1rem rgb(23,43,77,.14)">
              <div class="card-header border-0 p-3" style="border-radius: 1.25rem;">
                <h3 class="mb-0 d-flex align-items-center">Tanggapan</h3>
              </div>
              <div class="card-body pt-0">
                <form action="{{  Auth::user()->role[0]->role_name == "Petugas" ? route('petugas.tanggapan') : route('tanggapan')}} " method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id_pengaduan" value="{{ $pengaduan->id_pengaduan }}">
                  <!-- Tanggapan -->
                  
                    <div class="form-group">
                      <label class="form-control-label" for="status">Status</label>
                        <select name="status" class="form-control" id="status">
                            @if ($pengaduan->status == 'pending')
                                <option selected value="pending">Pending</option>
                                <option value="proses">Proses</option>
                                <option value="selesai">Selesai</option>
                            @elseif($pengaduan->status == 'proses')
                                <option value="pending">Pending</option>
                                <option selected value="proses">Proses</option>
                                <option value="selesai">Selesai</option>
                            @else
                                <option value="pending">Pending</option>
                                <option value="proses">Proses</option>
                                <option selected value="selesai">Selesai</option>
                            @endif
                        </select>
                      </div>
                    <div class="form-group">
                      <label class="form-control-label">Tanggapan</label>
                      <textarea rows="4" class="form-control" name="tanggapan" id="tanggapan" placeholder="Ketik tanggapan">{{ $tanggapan->tanggapan ?? '' }}</textarea>
                    </div>
                 
                    <div class="form-group mb-3">
                      <div class="d-flex justify-content-between mb-2">
                        <label for="foto" class="form-control-label m-0 pt-2">Foto Bukti</label>
                        <button id="tambah" class="btn py-1 text-white" style="background-color: #454696;"><i class="fas fa-plus"></i></button>
                      </div>
                      <input type="file" name="foto[]" id="foto" class="form-control @error('file') is-invalid @enderror" required>
                      @error('file')
                      <div class="invalid-feedback">
                        {{ $message }}
                      </div>
                      @enderror
                    </div>
                    <div id="tambahan">
                    </div>
                  <button type="submit" class="btn text-white" style="background-color: #454696;">Kirim</button>
                </form>
              </div>
            </div>
          </div>
          @endif
        </div>
      </div>
@endsection

@push('addon-script')
<script>
  document.getElementById("tambah").addEventListener("click", function(){
    let elemenUpload = `
      <div class="form-group mb-3">
          <label for="foto" class="form-control-label m-0 pt-2">Foto Bukti</label>
          <input type="file" name="foto[]" id="foto" class="form-control @error('file') is-invalid @enderror" required>
          @error('file')
              <div class="invalid-feedback">
                  {{ $message }}
              </div>
          @enderror
      </div>
    `;

    document.getElementById("tambahan").innerHTML += elemenUpload;
  });
</script>
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#pengaduanTable').DataTable();
    } );
</script>
@if (session()->has('status'))
<script>
    Swal.fire({
        title: 'Pemberitahuan!',
        text: "{{ Session::get('status') }}",
        icon: 'success',
        confirmButtonColor: '#454696',
        confirmButtonText: 'OK',
    });
    </script>
@endif
@endpush
