@extends('layouts.admin')
@section('title', 'Tambah Petugas')


@push('addon-style')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
@endpush
@section('content')
    <!-- Header -->
      <div class="container-fluid p-4 py-4">
              <h2 class=" d-inline-block mb-4">Tambah Petugas</h2>
              {{-- <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Petugas</a></li>
                </ol>
              </nav> --}}
          <!-- Page content -->
          <div class="container-fluid">
              <div class="row">
                <div class="col-12">
                  <div class="card"  style="border-radius: 1.25rem; box-shadow: 0 .1rem 1rem rgb(23,43,77,.14)">
                    <div class="card-body">
                      <form action="{{ route('admin.petugas.store')}} " method="POST">
                        @csrf                
                        <!-- Petugas -->
                        <div class="row">
                          <div class="col-4">
                            <div class="form-group">
                                <label class="form-control-label">Username</label>
                                <input type="text" value="{{ old('username') }}" class="form-control @error('username') is-invalid @enderror" name="username" id="username" required>
                                  @error('username')
                                  <div class="invalid-feedback">
                                      {{ $message }}
                                  </div>
                                  @enderror
                            </div>
                          </div>
                          <div class="col-4">
                            <div class="form-group">
                              <label class="form-control-label">Nama Petugas</label>
                              <input type="text" class="form-control" name="nama_petugas" id="nama_petugas" required>
                            </div>
                          </div>


                          <div class="col-4">
                            <div class="form-group">
                                <label class="form-control-label">Password</label>
                                <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password" required>
                                  @error('password')
                                  <div class="invalid-feedback">
                                          {{ $message }}
                                      </div>
                                  @enderror
                            </div>
                          </div>

                          <div class="col-5">
                            <div class="form-group">
                                <label class="form-control-label">No Telpon</label>
                                <input type="text" value="{{ old('telp') }}" class="form-control @error('telp') is-invalid @enderror" name="telp" id="telp" required>
                                @error('telp')
                                  <div class="invalid-feedback">
                                    {{ $message }}
                                  </div>
                                @enderror
                            </div>
                          </div> 
                          
                          <div class="col-5">
                            <div class="form-group">
                                <label class="form-control-label">Roles</label>
                                <select name="roles" id="roles" class="custom-select" required>
                                    <option value="" selected disabled>Pilih Roles</option>
                                    <option value="3">Petugas</option>
                                    <option value="2">Admin</option>
                                </select>
                            </div>
                          </div>
                        </div>


                        <button type="submit" class="btn text-white" style="background-color: #454696;">Simpan</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
          </div>
      </div>
@endsection

@push('addon-script')
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#pengaduanTable').DataTable();
    } );
</script>
@if (session()->has('status'))
<script>
    Swal.fire({
        title: 'Pemberitahuan!',
        text: "{{ Session::get('status') }}",
        icon: 'success',
        confirmButtonColor: '#454696',
        confirmButtonText: 'OK',
    });
    </script>
@endif
@endpush
