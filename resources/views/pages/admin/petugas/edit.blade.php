@extends('layouts.admin')
@section('title', 'Edit Petugas')


@push('addon-style')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
@endpush
@section('content')
    <!-- Header -->
    
      <div class="container-fluid p-4">
              <h2 class="d-inline-block mb-4">Edit Petugas</h2>
              {{-- <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Petugas</a></li>
                </ol>
              </nav> --}}
        
        <!-- Page content -->
          <div class="row">
            <div class="col-12">
              <div class="card" style="border-radius: 2rem; box-shadow: 0 .1rem 1rem rgb(23,43,77,.14);">
                <div class="card-body">
                  <form action="{{ route('admin.petugas.update', $petugas->user_id)}} " method="POST">
                  @csrf
                  @method('PUT')
                    <!-- Petugas -->
                    <div class="row">
                      <div class="col-4">
                        <div class="form-group">
                          <label class="form-control-label">Nama Petugas</label>
                          <input type="text" value="{{ $petugas->nama_petugas}}" class="form-control" name="nama_petugas" id="nama_petugas" required>
                        </div>
                      </div>

                      <div class="col-4">
                        <div class="form-group">
                            <label class="form-control-label">Username</label>
                            <input type="text" value="{{ $petugas->user->username}}" class="form-control" name="username" id="username" required>
                        </div>
                      </div>

                      <div class="col-4">
                        <div class="form-group">
                            <label class="form-control-label">Password</label>
                            <input type="password" class="form-control" name="password" id="password" placeholder="abaikan jika tidak merubah password">
                        </div>
                      </div>

                      <div class="col-5">
                        <div class="form-group">
                            <label class="form-control-label">No Telpon</label>
                            <input type="text" value="{{ $petugas->telp}}" class="form-control" name="telp" id="telp" required>
                        </div>
                      </div>

                      <div class="col-5">
                        <div class="form-group">
                          <label class="form-control-label">Roles</label>
                          <select name="roles" id="roles" class="form-control">
                            <option value="2" {{ Auth::user()->role[0]->role_name == "Administrator" ? "selected" : "" }}>Admin</option>
                            <option value="3" {{ Auth::user()->role[0]->role_name == "Petugas" ? "selected" : "" }}>Petugas</option>
                          </select>
                      </div>
                      </div>
                  </div>


                    <button type="submit" class="btn text-white p-2" style="background-color: #454696;">Simpan</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
      </div>
@endsection

@push('addon-script')
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#pengaduanTable').DataTable();
    } );
</script>
@if (session()->has('status'))
<script>
    Swal.fire({
        title: 'Pemberitahuan!',
        text: "{{ Session::get('status') }}",
        icon: 'success',
        confirmButtonColor: '#454696',
        confirmButtonText: 'OK',
    });
    </script>
@endif
@endpush
