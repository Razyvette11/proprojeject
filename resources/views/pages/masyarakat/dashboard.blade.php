@extends('layouts.user')
@section('title', 'Dashboard')

@section('content')
        <div class="container-fluid  p-4 py-4">
            <h6 class="h2 d-inline-block mb-4">Dashboard</h6>
                <div class="row">
                    <div class="col-12 col-lg-6 col-xl">
                        <!-- Value  -->
                        <div class="card" style="border-radius: 1.5rem; box-shadow: 0 .1rem 1rem rgb(23,43,77,.14)">
                            <div class="card-body p-3">
                                <div class="row align-items-center gx-0">
                                    <div class="col">
                                        <!-- Title -->
                                        <h6 class="text-uppercase text-muted mb-0">Semua Pengaduan</h6>
                                        <!-- Heading -->
                                        <span class="h2 mb-0">{{ $pengaduan }}</span>
                                    </div>
                                    <div class="col-auto">
                                        <!-- Icon -->
                                        <i class="fas fa-bullhorn" style="color: red; font-size: 20px;"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-lg-6 col-xl">
                        <!-- Value  -->
                        <div class="card" style="border-radius: 1.5rem; box-shadow: 0 .1rem 1rem rgb(23,43,77,.14)">
                            <div class="card-body p-3">
                                <div class="row align-items-center gx-0">
                                    <div class="col">
                                        <!-- Title -->
                                        <h6 class="text-uppercase text-muted mb-0">Diproses</h6>
                                        <!-- Heading -->
                                        <span class="h2 mb-0">{{ $proses }}</span>
                                    </div>
                                    <div class="col-auto">
                                        <!-- Icon -->
                                        <i class="fas fa-sync" style="color: coral; font-size: 20px;"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-lg-6 col-xl">
                        <!-- Value  -->
                        <div class="card" style="border-radius: 1.5rem; box-shadow: 0 .1rem 1rem rgb(23,43,77,.14)">
                            <div class="card-body p-3">
                                <div class="row align-items-center gx-0">
                                    <div class="col">
                                        <!-- Title -->
                                        <h6 class="text-uppercase text-muted mb-0">Selesai</h6>
                                        <!-- Heading -->
                                        <span class="h2 mb-0">{{ $selesai }}</span>
                                    </div>
                                    <div class="col-auto">
                                        <!-- Icon -->
                                        <i class="fas fa-check-circle" style="color: #2dce89; font-size: 20px;"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
       
            {{-- looping 3 data aduan yang terbaru --}}
        
            <div class="row mt-3">
            
                  @forelse($aduan->slice(0,3) as $i)
                  @php
                      // dd($aduan)
                  @endphp
                    <div class="col-md-4 mb-3">
                      <div class="card h-100" style="border-radius: 1.5rem; box-shadow: 0 .1rem 1rem rgb(23,43,77,.14)">
                        <img src="{{ Storage::url($i->foto) }}" class="card-img-top p-3" alt="..." height="213" style="border-radius:1.5rem;">
                        <div class="card-body">
                          <h3 class="card-title"><b>{{ $i->judul_laporan }}</b></h3>
                          <p class="card-text">{{Str::limit($i->isi_laporan, 85) }}</p>
                        </div>
                        <div class="card-footer bg-white pt-0 pb-3" style="border-radius:1.5rem;">
                            <div class="col-12 d-flex justify-content-end p-0">
                              <small class="text-muted">{{ Carbon\Carbon::parse($i->tgl_kejadian)->format('d F Y') }}</small>
                            </div>
                          </div> 
                      </div>
                    </div>
                  @empty
                  @endforelse               
            </div>
        </div>  
@endsection