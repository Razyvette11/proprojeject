@extends('layouts.user')

@section('title', 'Pengaduan')

@section('content')
<main id="main" class="martop">

    <section class="inner-page">
      <div class="container">
        <div class="title py-4 px-4">
            <h2 class="fw-bold">Detail Aduan</h2>
        </div>

        <div class="row px-4">
            <div class="col-md-6">
                <div class="card p-4 border-0" style="border-radius: 1.5rem; box-shadow: 0 .1rem 1rem rgb(23,43,77,.14)">
                    <div class="card-header text-center border-0 pb-0 m-0" style="border-radius: 1.5rem;">
                        <h3 class="text-bold">Data Pelapor</h3>
                        <hr style="border-color: #CAC4FF">
                    </div>
                    {{-- <p>
                            
                        {{ $pengaduan ? $pengaduan->user->name : "" }} <br>
                        {{ Carbon\Carbon::parse($pengaduan ? $pengaduan->tgl_kejadian : "")->format('d F Y') }} <br><br>
                        <h3>{{ $pengaduan ? $pengaduan->judul_laporan : "" }}</h3>
                            <p>{{ $pengaduan ? $pengaduan->status : "" }}</p>
                        
                        @if($tanggapan != null)
                        @foreach ( $tanggapan->uploads as $t )
                        <img src="{{ Storage::url($t ? $t->photo : "") }}" alt="image placeholder" style="margin-bottom: 15px;" height="250px">
                        @endforeach
                        @endif
                    </p> --}}
                    <div class="card-body">
                        <div class="col-12 pb-4">
                            <table class="table-tanggapan">
                                <tbody>
                                    <tr>
                                        <th>Username</th>
                                        <td>:</td>
                                        <td>{{ $pengaduan ? $pengaduan->user->name : "" }}</td>
                                    </tr>
                                    <tr>
                                        <th>Tanggal Kejadian</th>
                                        <td>:</td>
                                        <td>{{ Carbon\Carbon::parse($pengaduan ? $pengaduan->tgl_kejadian : "")->format('d F Y') }}</td>
                                    </tr>
                                    <tr>
                                        <th>Judul Pengaduan</th>
                                        <td>:</td>
                                        <td>{{ $pengaduan ? $pengaduan->judul_laporan : "" }}</td>
                                    </tr>
                                    <tr>
                                        <th>Status</th>
                                        <td>:</td>
                                        <td>{{ $pengaduan ? $pengaduan->status : "" }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-12 p-0">
                            @if($tanggapan != null)
                            @foreach ( $tanggapan->uploads as $t )
                            <div class="col-12 text-center pb-2 text-bold">
                                <span>Bukti Proses</span>
                            </div>
                                <img src="{{ Storage::url($t ? $t->photo : "") }}" alt="image placeholder" style="margin-bottom: 15px;" height="250px" width="100%">
                            @endforeach
                            @endif
                        </div>
                    </div>
            </div>
            </div>
                <div class="col-md-6">
                <div class="card p-4 border-0" style="border-radius: 1.5rem; box-shadow: 0 .1rem 1rem rgb(23,43,77,.14)">
                    {{-- <img src="{{ $pengaduan->foto }}" alt=""> --}}
                    <img src="{{ Storage::url($pengaduan ? $pengaduan->foto : "") }}" alt="Tidak Ada Bukti Foto yang dimasukkan" style="margin-bottom: 15px;" height="250px">
                    <div class="col-12 text-center pb-2 text-bold">
                        <span>Isi Laporan</span>
                    </div>
                    <p style="text-align: justify;">{{ $pengaduan ? $pengaduan->isi_laporan : "" }}</p>
                        
                </div>
                </div>
        </div>



      </div>
    </section>

  </main><!-- End #main -->
@endsection

<!-- @push('addon-script')
    @if (Auth::user()->role[0]->role_name != "Masyarakat")
        <script>
            Swal.fire({
                title: 'Peringatan!',
                text: "Anda harus login terlebih dahulu!",
                icon: 'warning',
                confirmButtonColor: '#28B7B5',
                confirmButtonText: 'Masuk',
                allowOutsideClick: false
                }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = '{{ route('user.masuk') }}';
                }else{
                    window.location.href = '{{ route('user.masuk') }}';
                }
                });
        </script>
    {{-- @elseif(auth('masyarakat')->user()->email_verified_at == null && auth('masyarakat')->user()->telp_verified_at == null) --}}
        <script>
            Swal.fire({
                title: 'Peringatan!',
                text: "Akun belum diverifikasi!",
                icon: 'warning',
                confirmButtonColor: '#28B7B5',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
                }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = '{{ route('user.masuk') }}';
                }else{
                    window.location.href = '{{ route('user.masuk') }}';
                }
                });
        </script>
    @endif

    @if (session()->has('pengaduan'))
        <script>
            Swal.fire({
                title: 'Pemberitahuan!',
                text: '{{ session()->get('pengaduan') }}',
                icon: '{{ session()->get('type') }}',
                confirmButtonColor: '#28B7B5',
                confirmButtonText: 'OK',
            });
        </script>
    @endif
@endpush -->