@extends('layouts.user')

@section('title', 'Laporan')

@section('content')
    <section class="inner-page">
      <div class="container ">
        <div class="title py-4 px-4">
            <h2 class="fw-bold">Pengaduan Saya</h2>
        </div>
        
            <div class="row px-4">
                @forelse($pengaduan as $i)
                <div class="col-md-4 pb-3">
                    <div class="card h-100 mb-0" style="border-radius: 1.5rem; box-shadow: 0 .1rem 1rem rgb(23,43,77,.14)">
                      <img src="{{ Storage::url($i->foto) }}" class="card-img-top p-3" alt="..." height="213" style="border-radius:1.5rem;">
                        <div class="card-body">
                            <h3 class="card-title"><b>{{ $i->judul_laporan }}</b></h3>
                            <p class="card-text m-0 pt-2">{{Str::limit($i->isi_laporan, 85) }}</p>
                        </div>
                        <div class="card-footer bg-white pt-0 pb-3" style="border-radius:1.5rem;">
                          <div class="col-12 d-flex justify-content-between pt-3 px-0">
                            <a href="{{ route('masyarakat.pengaduan.detail', $i->id_pengaduan) }}" class="btn text-white" style="background-color: #454696;">Detail</a>
                            <small class="text-muted p-2">{{ Carbon\Carbon::parse($i->tgl_kejadian)->format('d F Y') }}</small>
                        </div>
                    </div> 
                </div>
            </div>
                @empty
                @endforelse
              </div>
        

      </div>
    </section>  
@endsection
