@extends('layouts.user')

@section('title', 'Pengaduan')

@section('content')

      <div class="container-fluid  p-4 py-4">
            <h2 class="fw-bold mb-4">Form Aduan</h2>
            {{-- <h3 class="fw-normal">Sampaikan laporan Anda langsung kepada instansi pemerintah berwenang</h3> --}}
        
            <div class="card p-4 border-0 col-12" style="border-radius: 1.5rem; box-shadow: 0 .1rem 1rem rgb(23,43,77,.14)">
                <form action="{{ route('masyarakat.pengaduan.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group mb-3">
                                <label for="judul_laporan" class="form-label" style="color: #454696">Judul Laporan</label>
                                <input type="text" value="{{ old('judul_laporan') }}" name="judul_laporan" id="judul_laporan"
                                    placeholder="Ketik Judul Pengaduan" class="form-control @error('judul_laporan') is-invalid @enderror" required >
                                @error('judul_laporan')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label for="isi_laporan" class="form-label" style="color: #454696">Isi Laporan</label>
                                <textarea name="isi_laporan" id="isi_laporan"
                                    placeholder="Ketik isi Pengaduan" rows="5" class="form-control @error('isi_laporan') is-invalid @enderror" required>{{ old('isi_laporan') }}</textarea>
                                @error('isi_laporan')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label for="lokasi_kejadian" class="form-label" style="color: #454696">Lokasi Kejadian</label>
                                <textarea name="lokasi_kejadian" id="lokasi_kejadian"
                                    placeholder="Ketik Lokasi Kejadian" rows="3" class="form-control @error('lokasi_kejadian') is-invalid @enderror" required>{{ old('lokasi_kejadian') }}</textarea>
                                @error('lokasi_kejadian')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label for="tgl_kejadian" class="form-label" style="color: #454696">Tanggal Kejadian</label>
                                <input type="date" value="{{ old('tgl_kejadian') }}" name="tgl_kejadian" id="tgl_kejadian"
                                    placeholder="Tanggal Kejadian" class="form-control @error('tgl_kejadian') is-invalid @enderror" required
                                    >
                                @error('tgl_kejadian')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label for="id_kategori" class="form-label" style="color: #454696">Kategori Kejadian</label>
                                <select name="id_kategori" id="id_kategori" class="form-control" required>
                                    <option value="" selected disabled>Pilih Kategori Kejadian</option>
                                        @foreach($kategori as $kat)
                                        <option value="{{ $kat->id_kategori }} ">{{ $kat->nama_kategori}} </option>
                                        @endforeach
                                </select>
                                @error('id_kategori')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group mb-3">
                                <label for="foto" class="form-label" style="color: #454696">Foto Bukti</label>
                                <input type="file" name="foto" id="foto" class="form-control @error('file') is-invalid @enderror" required>
                                @error('file')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-md text-white" style="background-color: #454696">Kirim</button>

                </form>
            </div>
      </div>


@endsection

@push('addon-script')
    @if (Auth::user()->role[0]->role_name != "Masyarakat")
        <script>
            Swal.fire({
                title: 'Peringatan!',
                text: "Anda harus login terlebih dahulu!",
                icon: 'warning',
                confirmButtonColor: '#454696',
                confirmButtonText: 'Masuk',
                allowOutsideClick: false
                }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = '{{ route('user.masuk') }}';
                }else{
                    window.location.href = '{{ route('user.masuk') }}';
                }
                });
        </script>
    {{-- @elseif(auth('masyarakat')->user()->email_verified_at == null && auth('masyarakat')->user()->telp_verified_at == null)
        <script>
            Swal.fire({
                title: 'Peringatan!',
                text: "Akun belum diverifikasi!",
                icon: 'warning',
                confirmButtonColor: '#28B7B5',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
                }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = '{{ route('user.masuk') }}';
                }else{
                    window.location.href = '{{ route('user.masuk') }}';
                }
                });
        </script> --}}
    @endif

    @if (session()->has('pengaduan'))
        <script>
            Swal.fire({
                title: 'Pemberitahuan!',
                text: '{{ session()->get('pengaduan') }}',
                icon: '{{ session()->get('type') }}',
                confirmButtonColor: '#454696',
                confirmButtonText: 'OK',
            });
        </script>
    @endif
@endpush
