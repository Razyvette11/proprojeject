<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>Register | Pengaduan</title>

  @stack('prepend-style')
  @include('includes.admin.style')
  @stack('addon-style')
</head>

<body class="bg-white">
  <div class="container">
    <div class="row">
      <div class="col-md-6 pt-5">
        <img src="assets/img/undraw_remotely_2j6y.svg" alt="Image" class="img-fluid mt-5 px-3">
      </div>
      <div class="col-md-6 pt-5 px-5">
        <div class="mb-4">
          <h3>Register</h3>
          <p class="mb-4">Silahkan isi form dibawah ini untuk membuat akun baru.</p>
          <form action="{{ route('register') }}" role="form" method="POST">
            @csrf
            <div class="row px-3">
                <div class="col-6 mb-3 p-1">
                    <div class="input-group-prepend">
                      <label for="password" style="font-size: .856rem;">Nama</label>
                    </div>
                    <input type="text" value="{{ old('name') }}" class="form-control @error('name') is-invalid @enderror" name="name" id="name" placeholder="Nama">
                      @error('name')
                          <div class="invalid-feedback">
                              {{ $message }}
                          </div>
                      @enderror
                </div>
                <div class="col-6 mb-3 p-1">
                    <div class="input-group-prepend">
                      <label for="password" style="font-size: .856rem;">Username</label>
                    </div>
                    <input type="text" value="{{ old('username') }}" class="form-control @error('username') is-invalid @enderror" name="username" id="username" placeholder="Username">
                      @error('username')
                          <div class="invalid-feedback">
                              {{ $message }}
                          </div>
                      @enderror
                </div>
                <div class="col-12 mb-3 p-1">
                    <div class="input-group-prepend">
                      <label for="password" style="font-size: .856rem;">No Telpon</label>
                    </div>
                    <input type="text" value="{{ old('telp') }}" class="form-control @error('telp') is-invalid @enderror" name="telp" id="telp" placeholder="No Telpon">
                      @error('telp')
                          <div class="invalid-feedback">
                              {{ $message }}
                          </div>
                      @enderror
                  
                </div>
                <div class="col-12 mb-3 p-1">   
                    <div class="input-group-prepend">
                      <label for="password" style="font-size: .856rem;">Alamat</label>
                    </div>
                    <textarea type="text" class="form-control @error('address') is-invalid @enderror" name="address" id="address" placeholder="Alamat">{{ old('address') }}</textarea>
                      @error('address')
                          <div class="invalid-feedback">
                              {{ $message }}
                          </div>
                      @enderror
                  
                </div>
                <div class="col-6 mb-3 p-1">
                    <div class="input-group-prepend">
                      <label for="password" style="font-size: .856rem;">Password</label>
                    </div>
                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password" placeholder="Password">
                      @error('password')
                      <div class="invalid-feedback">
                              {{ $message }}
                          </div>
                      @enderror
                </div>
                <div class="col-6 mb-3 p-1">
                  <div class="input-group-prepend">
                    <label for="password" style="font-size: .856rem;">Konfirmasi Password</label>
                  </div>
                  <input type="password" class="form-control" name="password_confirmation" id="password" placeholder="Password" autocomplete="new-password">
                </div>
            </div>
            
            <div class="text-center">
              <button type="submit"  class="btn btn-block text-white" style="background-color: #454696">Buat Akun</button>
            </div>
            <div class="row mt-3">
              <div class="col text-right m-0">
                <a href="{{ url('login')}}" class="text-primary"><small>Sudah punya akun?</small></a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

  <!-- Argon Scripts -->
  @stack('prepend-script')
  @include('includes.admin.script')
  @stack('addon-script')
</body>

</html>
