<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect("home");
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

//---------------------------------------------------------------USER SET---------------------------------------------------------------------//

Route::middleware(['auth', 'role:Masyarakat'])->prefix('masyarakat')->group(function () {
    Route::get('/home', [\App\Http\Controllers\DashboardUserController::class,'index'])->name('masyarakat.home');
    Route::get('/pengaduan',  [\App\Http\Controllers\User\UserController::class, 'pengaduan'])->name('masyarakat.pengaduan');
    Route::get('/laporan/{who?}', [\App\Http\Controllers\User\UserController::class, 'laporan'])->name('masyarakat.pengaduan.laporan');
    Route::get('/pengaduan-detail/{id_pengaduan}', [\App\Http\Controllers\User\UserController::class, 'detailPengaduan'])->name('masyarakat.pengaduan.detail');
    Route::post('/pengaduan/kirim',  [\App\Http\Controllers\User\UserController::class, 'storePengaduan'])->name('masyarakat.pengaduan.store');
    Route::put('/edituserprofile/{id}', [\App\Http\Controllers\DashboardUserController::class, 'editProfile'])->name('masyarakat.edit');
});

//--------------------------------------------------------------ADMIN SET---------------------------------------------------------------------//

Route::middleware(['auth', 'role:Administrator'])->prefix('admin')->group(function () {
    Route::get('/home', [\App\Http\Controllers\DashboardUserController::class, 'admin'])->name('admin.home');
    //Data Petugas
    Route::get('/petugas', [\App\Http\Controllers\Admin\PetugasController::class,'index'])->name('admin.petugas.index');
    Route::get('/petugas/create', [\App\Http\Controllers\Admin\PetugasController::class,'create'])->name('admin.petugas.create');
    Route::post('/petugas/store', [\App\Http\Controllers\Admin\PetugasController::class,'store'])->name('admin.petugas.store');
    Route::get('/petugas/edit/{id}', [\App\Http\Controllers\Admin\PetugasController::class,'edit'])->name('admin.petugas.edit');
    Route::put('/petugas/update/{id}', [\App\Http\Controllers\Admin\PetugasController::class,'update'])->name('admin.petugas.update');
    Route::delete('/petugas/delete/{id}', [\App\Http\Controllers\Admin\PetugasController::class,'destroy'])->name('admin.petugas.destroy');

//Data Masyarakat
    Route::resource('/masyarakat', App\Http\Controllers\Admin\MasyarakatController::class);
    // Route::get('/masyarakat', [\App\Http\Controllers\Admin\MasyarakatController::class,'index'])->name('masyarakat.index');
    // Route::get('/masyarakat/{$id}', [\App\Http\Controllers\Admin\MasyarakatController::class,'show'])->name('masyarakat.show');
    // Route::delete('/masyarakat/delete',[\App\Http\Controllers\Admin\MasyarakatController::class,'destroy'])->name('masyarakat.destroy');

    Route::get('/laporan', [\App\Http\Controllers\Admin\LaporanController::class, 'index'])->name('laporan.index');
    Route::post('/laporan-get', [\App\Http\Controllers\Admin\LaporanController::class, 'laporan'])->name('laporan.get');
    Route::post('/laporan/export', [\App\Http\Controllers\Admin\LaporanController::class, 'export'])->name('laporan.export');
    Route::put('/editprofile/{id}', [\App\Http\Controllers\Admin\DashboardController::class, 'editProfile'])->name('edit');
    Route::get('pengaduan/{status}', [\App\Http\Controllers\Admin\PengaduanController::class, 'index'])->name('pengaduan.index');
    Route::get('pengaduan/show/{id_pengaduan}', [\App\Http\Controllers\Admin\PengaduanController::class, 'show'])->name('pengaduan.show');
    Route::delete('pengaduan/delete/{id_pengaduan}', [\App\Http\Controllers\Admin\PengaduanController::class, 'destroy'])->name('pengaduan.delete');
    // Tanggapan
    Route::post('tanggapan', [\App\Http\Controllers\Admin\TanggapanController::class, 'response'])->name('tanggapan');
    // Kategori
    Route::resource('kategori', \App\Http\Controllers\Admin\KategoriController::class);
    Route::get('pengaduan/user/{id_pengaduan}', [\App\Http\Controllers\PengaduanController::class, 'show'])->name('aduan.show');
});

//------------------------------------------------------------PETUGAS SET---------------------------------------------------------------------//

Route::middleware(['auth', 'role:Petugas'])->prefix('petugas')->group(function () {
    Route::get('/home', [\App\Http\Controllers\DashboardUserController::class, 'admin'])->name('petugas.home');
    //Data Petugas
    Route::get('/petugas', [\App\Http\Controllers\Petugas\PetugasController::class,'index'])->name('petugas.petugas.index');
    Route::get('/petugas/create', [\App\Http\Controllers\Petugas\PetugasController::class,'create'])->name('petugas.petugas.create');
    Route::post('/petugas/store', [\App\Http\Controllers\Petugas\PetugasController::class,'store'])->name('petugas.petugas.store');
    Route::get('/petugas/edit/{id}', [\App\Http\Controllers\Petugas\PetugasController::class,'edit'])->name('petugas.petugas.edit');
    Route::put('/petugas/update/{id}', [\App\Http\Controllers\Petugas\PetugasController::class,'update'])->name('petugas.petugas.update');
    Route::delete('/petugas/delete/{id}', [\App\Http\Controllers\Petugas\PetugasController::class,'destroy'])->name('petugas.petugas.destroy');
    //Data Masyarakat
    Route::resource('/masyarakat', App\Http\Controllers\Admin\MasyarakatController::class)->names([
        'index' => 'petugas.masyarakat.index',
        'show' => 'petugas.masyarakat.show',
        'destroy' => 'petugas.masyarakat.destroy'
    ]);
    // Route::get('/masyarakat', [\App\Http\Controllers\Petugas\MasyarakatController::class,'destroy'])->name('petugas.masyarakat.index');
    // Route::get('/masyarakat/{$id}', [\App\Http\Controllers\Petugas\MasyarakatController::class,'show'])->name('petugas.masyarakat.show');
    // Route::delete('/masyarakat/delete',[\App\Http\Controllers\Petugas\MasyarakatController::class,'destroy'])->name('petugas.masyarakat.destroy');


    Route::get('/laporan', [\App\Http\Controllers\Petugas\LaporanController::class, 'index'])->name('petugas.laporan.index');
    Route::post('/laporan-get', [\App\Http\Controllers\Petugas\LaporanController::class, 'laporan'])->name('petugas.laporan.get');
    Route::post('/laporan/export', [\App\Http\Controllers\Petugas\LaporanController::class, 'export'])->name('petugas.laporan.export');
    Route::put('/editprofile/{id}', [\App\Http\Controllers\Petugas\DashboardController::class, 'editProfile'])->name('petugas.edit');
    //Pengaduan 
    Route::get('pengaduan/{status}', [\App\Http\Controllers\Petugas\PengaduanController::class, 'index'])->name('petugas.pengaduan.index');
    Route::get('pengaduan/show/{id_pengaduan}', [\App\Http\Controllers\Petugas\PengaduanController::class, 'show'])->name('petugas.pengaduan.show');
    Route::delete('pengaduan/delete/{id_pengaduan}', [\App\Http\Controllers\Petugas\PengaduanController::class, 'destroy'])->name('petugas.pengaduan.delete');
        
    // Tanggapan
    Route::post('tanggapan', [\App\Http\Controllers\Petugas\TanggapanController::class, 'response'])->name('petugas.tanggapan');
        
    // Kategori
    Route::get('kategori', [\App\Http\Controllers\Petugas\KategoriController::class,'index'])->name('petugas.kategori.index');
    Route::get('kategori/create', [\App\Http\Controllers\Petugas\KategoriController::class,'create'])->name('petugas.kategori.create');
    Route::post('kategori/store', [\App\Http\Controllers\Petugas\KategoriController::class,'store'])->name('petugas.kategori.store');
    Route::get('kategori/edit/{id}', [\App\Http\Controllers\Petugas\KategoriController::class,'edit'])->name('petugas.kategori.edit');
    Route::put('kategori/update', [\App\Http\Controllers\Petugas\KategoriController::class,'update'])->name('petugas.kategori.update');
    Route::delete('kategori/delete', [\App\Http\Controllers\Petugas\KategoriController::class,'destroy'])->name('petugas.kategori.delete');
    Route::get('pengaduan/user/{id_pengaduan}', [\App\Http\Controllers\PengaduanController::class, 'show'])->name('petugas.aduan.show');
    
});

//override,authenticated, redirect, route dr msing" dashboard